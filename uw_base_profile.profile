<?php

/**
 * @file
 * Profile file.
 */

// Add global for $_uw_base_profile_install_from_db so it gets referenced
// correctly in a later function.
global $_uw_base_profile_install_from_db;
// Set to FALSE to create a new database export.
$_uw_base_profile_install_from_db = FALSE;

if ($_uw_base_profile_install_from_db) {
  include_once 'install_from_db/install_from_db.install.inc';
}

/**
 * This function is called by form submit handlers to increase execution time.
 */
function _uw_base_profile_max_execution_time() {
  // Increase execution time for installs.
  drupal_set_time_limit(0);

  // Make sure the MySQL does not die while scripts are executed.
  if (db_driver() === 'mysql') {
    db_query('SET SESSION wait_timeout = 480');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function uw_base_profile_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  // Pre-populate the country with "Canada".
  $form['server_settings']['site_default_country']['#default_value'] = 'CA';
  // Disable time zone autodetect and default to "America/Toronto".
  $form['server_settings']['date_default_timezone']['#attributes']['class'][0] = '';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'America/Toronto';
  // Disable email notifications by default.
  $form['update_notifications']['update_status_module']['#default_value'][1] = 0;
}

/**
 * Implements hook_install_tasks().
 */
function uw_base_profile_install_tasks(&$install_state) {
  $tasks = array();

  $tasks['uw_base_profile_install_site_controller'] = array(
    'display_name' => st('Site controller'),
    'type' => 'normal',
  );
  $tasks['uw_select_site_features_form'] = array(
    'display_name' => st('Features'),
    'type' => 'form',
  );
  $tasks['uw_create_default_content_form'] = array(
    'display_name' => st('Create content'),
    'type' => 'form',
  );
  $tasks['uw_install_uw_auth_wcms_admins'] = array(
    'display_name' => st('Set up administrators'),
    'type' => 'normal',
  );
  // Skip tasks of installing site controller and all interactive form if using
  // quickstart.
  if (!empty($install_state['parameters']['quickstart']) && $install_state['parameters']['quickstart'] == 'quick') {
    foreach ($tasks as $task_name => $task) {
      $tasks['uw_base_profile_install_site_controller']['run'] = INSTALL_TASK_SKIP;
      if (isset($tasks[$task_name]['type']) && $tasks[$task_name]['type'] == 'form') {
        $tasks[$task_name]['run'] = INSTALL_TASK_SKIP;
      }
    }
  }
  return $tasks;
}

/**
 * Implements hook_install_tasks_alter().
 */
function uw_base_profile_install_tasks_alter(&$tasks, $install_state) {
  global $_uw_base_profile_install_from_db;
  if ($_uw_base_profile_install_from_db) {
    install_from_db_install_tasks_alter($tasks, $install_state);
  }
}

/**
 * Task callback: installs the site controller.
 */
function uw_base_profile_install_site_controller() {
  $success = module_enable(array('uw_site_fdsu'));
  if (!$success) {
    return 'Could not enable site controller, a module dependency was missing -- the function module_enable() returned FALSE';
  }
}

/**
 * Return site config form items for use in GUI and automation form installs.
 *
 * The form returned here is included in the GUI install and in the install form
 * created by _uw_automationform_build_form(). For GUI installs, values are
 * passed by the Drupal installer directly to
 * uw_select_site_features_form_submit(). For Automation Form installs,
 * _uw_automationform_complete_submit() passes the values selected here to
 * Jenkins job Site-Create-script which passes them to site-create.php which
 * passes them to `drush site-install`, which ultimately passes them to
 * uw_select_site_features_form_submit(). Either way has the same result.
 *
 * In the Automation Form, the configuration available is based on the profile
 * that the Automation Form itself is running.
 *
 * @see https://jira.uwaterloo.ca/browse/ISTWCMS-3231
 *
 * @return array
 *   A form API array.
 */
function uw_base_profile_site_config_form() {
  $t = get_t();

  $form['uw_site_type'] = array(
    '#type' => 'select',
    '#title' => $t('Select the type of site:'),
    '#options' => array(
      'default' => $t('Standard FDSU'),
      'conference' => $t('Conference'),
      'publication' => $t('Publication'),
      'single_page' => $t('Single page'),
    ),
    '#default_value' => 'default',
  );

  require_once __DIR__ . '/themes/uw_fdsu_theme/theme-settings.php';
  $color_form = [];
  uw_fdsu_theme_form_system_theme_settings_alter($color_form, []);
  $form['uw_fdsu_theme_color_css'] = $color_form['color']['css'];

  $form['set_auth_type'] = array(
    '#type' => 'select',
    '#title' => $t('Select the authentication type:'),
    '#options' => array(
      'closed' => $t('Closed: do not create Drupal accounts for any CAS login'),
      'open' => $t('Open: create Drupal accounts for any CAS login'),
      'auth' => $t('Authentication Required Site: Configurable access control.'),
    ),
    '#default_value' => 'closed',
  );

  return $form;
}

/**
 * Task callback.
 *
 * Returns the form allowing the user to turn on features at install time. Made
 * the content types and dev. modules checked by default (#default_value =>
 * TRUE) to speed up site creation time for local development (Dec, 15 2012).
 *
 * @return array
 *   The form array.
 */
function uw_select_site_features_form() {
  drupal_set_title(st('Enable features'));

  $form['extra_config'] = uw_base_profile_site_config_form();

  $form['enable_content_types'] = array(
    '#type' => 'fieldset',
    '#title' => st('Enable content types'),
    '#description' => st('If the site type is not Standard FDSU, this configuration is ignored.'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  // The array key is the name of the module.
  $form['enable_content_types']['uw_ct_award'] = array(
    '#type' => 'checkbox',
    '#title' => st('Award'),
    '#default_value' => 0,
  );
  $form['enable_content_types']['uw_ct_bibliography'] = array(
    '#type' => 'checkbox',
    '#title' => st('Bibliography'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_blog'] = array(
    '#type' => 'checkbox',
    '#title' => st('Blog'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_catalog'] = array(
    '#type' => 'checkbox',
    '#title' => st('Catalog'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_contact'] = array(
    '#type' => 'checkbox',
    '#title' => st('Contacts'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_custom_listing'] = array(
    '#type' => 'checkbox',
    '#title' => st('Custom listing page'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_embedded_call_to_action'] = array(
    '#type' => 'checkbox',
    '#title' => st('Embedded call to action'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_embedded_facts_and_figures'] = array(
    '#type' => 'checkbox',
    '#title' => st('Embedded facts and figures'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_embedded_timeline'] = array(
    '#type' => 'checkbox',
    '#title' => st('Embedded timeline'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_event'] = array(
    '#type' => 'checkbox',
    '#title' => st('Events'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_home_page_banner'] = array(
    '#type' => 'checkbox',
    '#title' => st('Home page banners'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_image_gallery'] = array(
    '#type' => 'checkbox',
    '#title' => st('Image galleries'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_news_item'] = array(
    '#type' => 'checkbox',
    '#title' => st('News items'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_opportunities'] = array(
    '#type' => 'checkbox',
    '#title' => st('Opportunities'),
    '#default_value' => 0,
  );
  $form['enable_content_types']['uw_ct_person_profile'] = array(
    '#type' => 'checkbox',
    '#title' => st('People profiles'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_project'] = array(
    '#type' => 'checkbox',
    '#title' => st('Project'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_promo_item'] = array(
    '#type' => 'checkbox',
    '#title' => st('Promotional items'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_service'] = array(
    '#type' => 'checkbox',
    '#title' => st('Service'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_special_alert'] = array(
    '#type' => 'checkbox',
    '#title' => st('Special Alert'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_web_form'] = array(
    '#type' => 'checkbox',
    '#title' => st('Web Form'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_nav_site_footer'] = array(
    '#type' => 'checkbox',
    '#title' => st('Site Footer'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_captcha'] = array(
    '#type' => 'checkbox',
    '#title' => st('CAPTCHA'),
    '#default_value' => 1,
  );

  // Development.
  $form['enable_development'] = array(
    '#type' => 'fieldset',
    '#title' => st('Enable development modules'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['enable_development']['context_ui'] = array(
    '#type' => 'checkbox',
    '#title' => st('Context UI'),
    '#default_value' => 1,
  );
  $form['enable_development']['uw_cfg_devel'] = array(
    '#type' => 'checkbox',
    '#title' => st('Devel'),
    '#default_value' => 0,
  );
  $form['enable_development']['devel_generate'] = array(
    '#type' => 'checkbox',
    '#title' => st('Devel Generate'),
    '#default_value' => 0,
    '#states' => array(
      'visible' => array(
        ':input[name="enable_development[devel]"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['enable_development']['drupal_reset'] = array(
    '#type' => 'checkbox',
    '#title' => st('Drupal Reset'),
    '#default_value' => 0,
  );
  $form['enable_development']['module_filter'] = array(
    '#type' => 'checkbox',
    '#title' => st('Module filter'),
    '#default_value' => 1,
  );
  $form['enable_development']['views_ui'] = array(
    '#type' => 'checkbox',
    '#title' => st('Views UI'),
    '#default_value' => 1,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Enable and continue'),
  );

  return $form;
}

/**
 * Submit callback: creates the requested default content.
 */
function uw_select_site_features_form_submit(&$form, &$form_state) {
  _uw_base_profile_max_execution_time();

  $modules = array();

  if ($form_state['values']['uw_fdsu_theme_color_css'] !== 'default') {
    variable_set('uw_fdsu_theme_color_css', $form_state['values']['uw_fdsu_theme_color_css']);
  }

  if ($form_state['values']['set_auth_type'] === 'closed') {
    variable_set('cas_user_register', FALSE);
  }
  else {
    variable_set('cas_user_register', TRUE);
    if ($form_state['values']['set_auth_type'] === 'auth') {
      $modules['uw_auth_site'] = TRUE;
    }
  }

  if (!empty($form_state['values']['enable_content_types'])) {
    $modules = array_merge($modules, $form_state['values']['enable_content_types']);
  }

  if (!empty($form_state['values']['enable_development'])) {
    $modules = array_merge($modules, $form_state['values']['enable_development']);
  }

  $site_type = $form_state['values']['uw_site_type'];
  if ($site_type !== 'default') {
    // Disable all content type modules in a modules array.
    foreach (array_keys($modules) as $module) {
      if (substr($module, 0, 6) === 'uw_ct_') {
        $modules[$module] = FALSE;
      }
    }
    // Enable configuration for this site type.
    $modules['uw_cfg_' . $site_type] = TRUE;
  }

  _uw_base_profile_enable_modules($modules);
}

/**
 * Task callback: Returns the form allowing creation of default content.
 */
function uw_create_default_content_form() {
  drupal_set_title(st('Create initial default content'));

  if (module_exists('locale')) {

    // Currently there is only English.  This may change.
    $language_options = array(
      'en' => 'English',
      'und' => 'Undefined',
    );
    $form['install_profile_language'] = array(
      '#type' => 'fieldset',
      '#title' => st('Site content language (interface will be English)'),
      '#collapsible' => FALSE,
    );

    $form['install_profile_language']['langcode'] = array(
      '#type' => 'select',
      '#title' => st('Language'),
      '#options' => $language_options,
      '#default_value' => 'en',
      '#description' => t('Use English, unless you know what you are doing.'),
    );
  }

  // Default Web Pages.
  if (module_exists('uw_ct_web_page')) {
    // No default content for uw_theme_marketing sites.
    $is_uw_theme_marketing = variable_get('theme_default') === 'uw_theme_marketing';

    $form['add_default_pages'] = array(
      '#type' => 'fieldset',
      '#title' => st('Create default web pages'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );

    $form['add_default_pages']['home'] = array(
      '#type' => 'checkbox',
      '#title' => st('Home'),
      '#default_value' => !$is_uw_theme_marketing,
    );

    $form['add_default_pages']['about'] = array(
      '#type' => 'checkbox',
      '#title' => st('About'),
      '#default_value' => !$is_uw_theme_marketing,
    );
  }

  // Turn on term lock for profile and audience taxonomies.
  if (module_exists('uw_vocab_audience') || module_exists('uw_ct_person_profile')) {
    $form['add_vocab_pages'] = array(
      '#type' => 'fieldset',
      '#title' => st('Add terms to vocabularies'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );

    // Add Audience vocabulary terms.
    if (module_exists('uw_vocab_audience')) {
      $form['add_vocab_pages']['audience'] = array(
        '#type' => 'checkbox',
        '#title' => st('Audience Vocabulary Terms'),
        '#default_value' => 1,
      );
    }
    // Add Profile vocabulary terms.
    if (module_exists('uw_ct_person_profile')) {
      $form['add_vocab_pages']['profile'] = array(
        '#type' => 'checkbox',
        '#title' => st('Profile Vocabulary Terms'),
        '#default_value' => 1,
      );
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['submit'] = array('#type' => 'submit', '#value' => st('Continue'));

  return $form;
}

/**
 * Task callback: installs the uw_auth_wcms_admins module.
 */
function uw_install_uw_auth_wcms_admins() {
  _uw_base_profile_enable_modules(array('uw_auth_wcms_admins' => 1));
}

/**
 * Submit callback: creates the requested default content.
 */
function uw_create_default_content_form_submit(&$form, &$form_state) {
  _uw_base_profile_max_execution_time();

  // Clear all caches to rebuild the registry and node types that were setup
  // earlier.
  drupal_flush_all_caches();
  // Rebuild permissions for latest workbench_moderation module (April 28, 2015)
  node_access_rebuild();

  $modules = array();

  // Add default pages.
  if (!empty($form_state['values']['add_default_pages']) && array_filter($form_state['values']['add_default_pages'])) {
    // Put the default pages into a Drupal variable. When the module is enabled,
    // it will use this variable to determine which pages to add.
    variable_set('uw_data_web_pages_setup', $form_state['values']['add_default_pages']);
    $modules['uw_data_web_pages'] = 1;

  }
  if (!empty($form_state['values']['add_vocab_pages']['audience'])) {
    $modules['uw_data_audience_categories'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['profile'])) {
    $modules['uw_data_profile_categories'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awarddegree'])) {
    $modules['uw_data_academic_degree'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awardcitizenship'])) {
    $modules['uw_data_citizenship'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awarddepartment'])) {
    $modules['uw_data_department'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awardcategory'])) {
    $modules['uw_data_award_categories'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awardterm'])) {
    $modules['uw_data_award_term'] = 1;
  }
  // Add default location / writer tips.
  if (!empty($form_state['values']['add_default_content'])) {
    $modules = array_merge($modules, $form_state['values']['add_default_content']);
  }
  if (!empty($modules)) {
    _uw_base_profile_enable_modules($modules);
  }
  // No longer need profile language variable.
  // Update all content to the selected Language
  // Jan 16th 2015, Chose not to rewrite all data modules to use selected
  // language. Instead rewrite it all, as we did in uw_site_fdsu_update_7116().
  // This may need to be changed if the profile starts to support languages
  // other than english in inital install.
  if (!empty($form_state['values']['langcode'])) {
    $langcode = check_plain($form_state['values']['langcode']);
    _uw_base_profile_set_content_language($langcode);
  }
}

/**
 * Enable a bunch of modules.
 *
 * @param array $modules
 *   Given an array where key => module_name and value => TRUE/FALSE, enables
 *   those modules.
 */
function _uw_base_profile_enable_modules(array $modules = array()) {
  $module_list = array();
  foreach ($modules as $key => $value) {
    if ($value) {
      $module_list[] = $key;
    }
  }
  if (!empty($module_list)) {
    module_enable($module_list);
  }
}

/**
 * Set language for content.
 *
 * @param string $langcode
 *   The language code.
 */
function _uw_base_profile_set_content_language($langcode) {
  $install_languages = language_list();

  // Add English language if it does not exist (Probably do not need to check).
  if (!isset($install_languages['en'])) {
    locale_add_language('en');
    drupal_set_message('Adding English language.');
  }
  // Set default language.
  variable_set('language_default', $install_languages[$langcode]);

  // In enabled the FDSU site controller it sets the admin interface language to
  // English. Grab all translatable fields.
  $fields = db_select('field_config', 'fc')
    ->fields('fc')
    ->condition('translatable', 1, '=')
    // field_delete_field() does not remove fields from {field_config}. It just
    // marks them deleted. This is needed because of fields deleted in
    // uw_conference_admin_install().
    ->condition('deleted', 0, '=')
    ->execute();
  // Update each translatable field and its revisions.
  foreach ($fields as $field) {
    db_update('field_data_' . $field->field_name)
      ->fields(array('language' => $langcode))
      ->condition('language', 'und')
      ->execute();
    db_update('field_revision_' . $field->field_name)
      ->fields(array('language' => $langcode))
      ->condition('language', 'und')
      ->execute();
  }
  // Update all nodes.
  db_update('node')
    ->fields(array('language' => $langcode))
    ->condition('language', 'und')
    ->execute();
  // Update all url alias.
  db_update('url_alias')
    ->fields(array('language' => $langcode))
    ->condition('language', 'und')
    ->execute();
  // Update all comments.
  db_update('comment')
    ->fields(array('language' => $langcode))
    ->condition('language', 'und')
    ->execute();
  db_update('entity_translation')
    ->fields(array('language' => $langcode))
    ->condition('language', 'und')
    ->execute();
  db_update('entity_translation_revision')
    ->fields(array('language' => $langcode))
    ->condition('language', 'und')
    ->execute();
}

/**
 * Remove a message as set by drupal_set_message().
 *
 * This is used during install to remove irrelavent messages.
 */
function _uw_base_profile_remove_message($partial_message, $type = 'status') {
  if (!empty($_SESSION['messages'][$type])) {
    foreach ($_SESSION['messages'][$type] as $key => $message) {
      if (strpos($message, $partial_message) !== FALSE) {
        unset($_SESSION['messages'][$type][$key]);
      }
    }
    if (empty($_SESSION['messages'][$type])) {
      unset($_SESSION['messages'][$type]);
    }
  }
}

/**
 * Command to be run by the devops tools after install.
 *
 * Makes adjustments to main menu. If this is run as part of the normal install,
 * it doesn't work.
 */
function uw_base_profile_post_install() {
  // Load main-menu links.
  $links = menu_load_links('main-menu');
  // Store the mlid of the about page.
  $about_mlid = NULL;
  foreach ($links as $link) {
    if ($link['link_title'] === 'About ' . variable_get('site_name')) {
      $about_mlid = $link['mlid'];
      break;
    }
  }
  // Update menus as needed.
  $links_about = ['about/people', 'people-profiles'];
  $links_hidden = ['publications', 'services', 'projects', 'blog'];
  foreach ($links as $link) {
    // Name of home link.
    if ($link['link_title'] === 'Home') {
      $link['link_title'] = variable_get('site_name') . ' home';
      menu_link_save($link);
    }
    // Child items of about page.
    elseif ($about_mlid && in_array($link['link_path'], $links_about)) {
      $link['plid'] = $about_mlid;
      menu_link_save($link);
    }
    elseif (in_array($link['link_path'], $links_hidden)) {
      $link['hidden'] = 1;
      menu_link_save($link);
    }
  }
}
