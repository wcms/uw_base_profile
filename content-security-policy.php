<?php

/**
 * @file
 * Outputs the Content Security Policy headers used by the WCMS profile.
 *
 * This file is stored here for version control purposes. The headers need to be
 * configured as part of the web server configuration.
 */

$items = array(
  "'self'",

  // These are all bad for security, but they are needed for various things such
  // as MathJax.
  "'unsafe-inline'",
  "'unsafe-eval'",
  'data:',
);

$sites = array(
  // University of Waterloo.
  'uwaterloo.ca',
  '*.uwaterloo.ca',
  // Bootstrap.
  'maxcdn.bootstrapcdn.com',
  // Google.
  '*.google.com',
  '*.google-analytics.com',
  '*.googleapis.com',
  '*.gstatic.com',
  '*.googletagmanager.com',
  '*.googleusercontent.com',
  // Additional Google. RT#584058.
  '*.googleadservices.com',
  // Google Analytics.
  '*.g.doubleclick.net',
  // Google's DoubleClick, for MSI's use.
  '6263835.fls.doubleclick.net',
  // WCMS-3314, more DoubleClick.
  'ad.doubleclick.net',
  '14868352.fls.doubleclick.net',
  // CDNjs (Cloudflare).
  'cdnjs.cloudflare.com',
  // Twitter. Last one from RT#1139651.
  'twitter.com',
  '*.twitter.com',
  '*.twimg.com',
  'twitter-widgets.s3.amazonaws.com',
  'static.ads-twitter.com',
  // FaceBook.
  '*.facebook.com',
  '*.facebook.net',
  // Additional FaceBook / Instagram. RT#1028033.
  'scontent.xx.fbcdn.net',
  // YouTube.
  '*.youtube.com',
  '*.youtube-nocookie.com',
  // Additional Youtube. RT#922158.
  's.ytimg.com',
  // Livestream.
  '*.livestream.com',
  // WebSpellChecker.net.
  '*.webspellchecker.net',
  // MathJax.
  'cdn.mathjax.org',
  // AddToAny.
  '*.addtoany.com',
  // Vimeo.
  '*.vimeo.com',
  '*.vimeocdn.com',
  // Tint.
  '*.tintup.com',
  // Tint; RT#456523.
  '*.71n7.com',
  'd36hc0p18k1aoc.cloudfront.net',
  // Tint: RT#916449. (Hypemarks *is* Tint, so *maybe* the Cloudfront exception
  // above is no longer needed.
  'cdn.hypemarks.com',
  // Maps.
  'cdn.leafletjs.com',
  'cdn-geoweb.s3.amazonaws.com',
  'cdn.maptiks.com',
  'api.tiles.mapbox.com',
  'd591zijq8zntj.cloudfront.net',
  // LibAnswers. RT#429697.
  '*.libanswers.com',
  // LibChat. RT#979378.
  '*.libchat.com',
  // Skype. RT#429697.
  'secure.skype.com',
  // Domains required for Lightning Bolt, used by MSI. RT#563158.
  'cdn-akamai.mookie1.com',
  '*.tiqcdn.com',
  // Eyereturn, used by CBET. RT#569453.
  'o2.eyereturn.com',
  // LinkedIn. RT#580093, RT#727427.
  'snap.licdn.com',
  '*.ads.linkedin.com',
  // Hootsuite Campaigns. RT#582824.
  '*.hscampaigns.com',
  // Used by MSI, confirmed by Joe Kwan.
  'secure.adnxs.com',
  // Used by Tableau.
  'public.tableau.com',
  // CodePen embeds.
  'static.codepen.io',
  'codepen.io',
  // Power BI app, ISTWCMS-3381.
  'app.powerbi.com',
  // Yahoo, RT#1110769.
  's.yimg.com',
  'sp.analytics.yahoo.com',
  // Social Intents, ISTWCMS-4480.
  'chat.socialintents.com',
  // hCaptcha, ISTWCMS-4881.
  'hcaptcha.com',
  'newassets.hcaptcha.com',
  // Campus Map, possibly required due to Pantheon switchover.
  'code.jquery.com',
  'experience.arcgis.com',
  // ISTWCMS-4929: leaflet API.
  'api.mapbox.com',
  // ISTWCMS-4617: Instagram.
  'www.instagram.com',
  // Pantheon AGCDN configuration (wcms-tools).
  'vuejs.org',
  'cdn.jsdelivr.net',
  'js-agent.newrelic.com',
  'bam.nr-data.net',
  'us-central1-pantheon-psapps.cloudfunctions.net',
  // Unknown reason, but these were on Pantheon so better not remove them.
  'cdn.ckeditor.com',
  'netdna.bootstrapcdn.com',
  // HotJar, ISTWCMS-5461.
  // https://help.hotjar.com/hc/en-us/articles/115011640307-Content-Security-Policies.
  // We do not allow HTTP as they recommend, and we do not currently
  // support wss:// - will see if that causes problems.
  '*.hotjar.com',
  '*.hotjar.io',
  // Kuali, ISTWCMS-6580.
  '*.kuali.co',
  // Mailchimp OAuth
  'drupal-oauth.mailchimp.com',
  // StackAdapt, WCMS-3654
  'srv.stackadapt.com',
  '*.srv.stackadapt.com',
  'qvdt3feo.com',
);
// Allow an external file for local CSP exemptions that shouldn't be globally
// committed.
if (file_exists('local-content-security-policy.php')) {
  require_once 'local-content-security-policy.php';
  $sites = array_merge($sites, $local_sites);
}
while ($site = array_shift($sites)) {
  $items[] = 'https://' . $site;
}

$img_items = array(
  '*',
  'data:',
);

echo 'default-src ' . implode(' ', $items) . '; img-src ' . implode(' ', $img_items) . "\n";
