#!/bin/sh
rm -rf libraries modules themes
echo Building site profile...
drush make --working-copy --force-gitinfofile --no-recursion --no-core -y --contrib-destination=. uw_base_profile.make
chmod -R g+w *
# Reset info file then add version and other packaging information.
git checkout uw_base_profile.info
printf "

; Information added by rebuild.sh on %b.
version = \"%b\"
project = \"%b\"
datestamp = \"%b\"
" `date +%Y-%m-%d` `git describe --tags --always` uw_base_profile `date +%s` >> uw_base_profile.info
