; uWaterloo Base Profile Make File

core = 7.x
api = 2

;------------------------------------------------------------------------------------------------------------------------
; HOWTO specify what version of a contrib module to install
;
; Use one of the following options. Do not make any branches, tags, or
; commits to our copy of a drupal.org repository unless making a
; locally-modified version.
;
; 1. Refer to a tag that exists on drupal.org:
;    projects[better_exposed_filters][download][tag] = "7.x-3.2"
;
; 2. Refer to a branch and revision that exists on drupal.org:
;    projects[better_formats][download][branch] = "7.x-1.x"
;    projects[better_formats][download][revision] = "3b4a8c9"
;
; 3. Use a locally-modified version. Refer to a local tag on a local
;    branch of a drupal.org project and explain the changes in the
;    comments. For details see: https://collaborate.uwaterloo.ca/node/520
;    ; GMap
;    ; Base: 7.x-2.10
;    ; Patch 4a148f7: Make pop-ups work. https://www.drupal.org/node/2445429#comment-10314603
;    projects[gmap][type] = "module"
;    projects[gmap][download][type] = "git"
;    projects[gmap][download][url] = "https://git.uwaterloo.ca/drupal-org/gmap.git"
;    projects[gmap][download][tag] = "7.x-2.10-uw_wcms1"
;    projects[gmap][subdir] = "contrib"
;
;------------------------------------------------------------------------------------------------------------------------

;------------------------------------------------------------------------------------------------------------------------
; Contrib Modules
; Contributed modules from Drupal.org
;------------------------------------------------------------------------------------------------------------------------

; Administration Language
projects[admin_language][type] = "module"
projects[admin_language][download][type] = "git"
projects[admin_language][download][url] = "https://git.uwaterloo.ca/drupal-org/admin_language.git"
projects[admin_language][download][tag] = "7.x-1.0"
projects[admin_language][subdir] = "contrib"

; Administration Views
projects[admin_views][type] = "module"
projects[admin_views][download][type] = "git"
projects[admin_views][download][url] = "https://git.uwaterloo.ca/drupal-org/admin_views.git"
projects[admin_views][download][tag] = "7.x-1.8"
projects[admin_views][subdir] = "contrib"

; Autocomplete Deluxe
projects[autocomplete_deluxe][type] = "module"
projects[autocomplete_deluxe][download][type] = "git"
projects[autocomplete_deluxe][download][url] = "https://git.uwaterloo.ca/drupal-org/autocomplete_deluxe.git"
projects[autocomplete_deluxe][download][tag] = "7.x-2.3"
projects[autocomplete_deluxe][subdir] = "contrib"

; Better Exposed Filters
projects[better_exposed_filters][type] = "module"
projects[better_exposed_filters][download][type] = "git"
projects[better_exposed_filters][download][url] = "https://git.uwaterloo.ca/drupal-org/better_exposed_filters.git"
projects[better_exposed_filters][download][tag] = "7.x-3.6"
projects[better_exposed_filters][subdir] = "contrib"

; Better Formats
projects[better_formats][type] = "module"
projects[better_formats][download][type] = "git"
projects[better_formats][download][url] = "https://git.uwaterloo.ca/drupal-org/better_formats.git"
projects[better_formats][download][tag] = "7.x-1.0-beta2"
projects[better_formats][subdir] = "contrib"

; Biblio
projects[biblio][type] = "module"
projects[biblio][download][type] = "git"
projects[biblio][download][url] = "https://git.uwaterloo.ca/drupal-org/biblio.git"
projects[biblio][download][tag] = "7.x-1.5"
projects[biblio][subdir] = "contrib"

; Calendar
; Base: 6164043 (7.x-3.5+1-dev)
; Patch 4d22059: Fields are displayed even if "Exclude from display" is checked. https://www.drupal.org/project/calendar/issues/1539376#comment-12792735
projects[calendar][type] = "module"
projects[calendar][download][type] = "git"
projects[calendar][download][url] = "https://git.uwaterloo.ca/drupal-org/calendar.git"
projects[calendar][download][tag] = "7.x-3.6"
projects[calendar][subdir] = "contrib"

; CAPTCHA
projects[captcha][type] = "module"
projects[captcha][download][type] = "git"
projects[captcha][download][url] = "https://git.uwaterloo.ca/drupal-org/captcha.git"
projects[captcha][download][tag] = "7.x-1.7"
projects[captcha][subdir] = "contrib"

; CAS
; Base: 8cfb322 (7.x-1.7)
; Patch 80eeb55: With JS disabled, cannot log in via CAS in login block. https://www.drupal.org/project/cas/issues/3018239#comment-12888434
projects[cas][type] = "module"
projects[cas][download][type] = "git"
projects[cas][download][url] = "https://git.uwaterloo.ca/drupal-org/cas.git"
projects[cas][download][tag] = "7.x-1.7-uw_wcms1"
projects[cas][subdir] = "contrib"

; Charts
projects[charts][type] = "module"
projects[charts][download][type] = "git"
projects[charts][download][url] = "https://git.uwaterloo.ca/drupal-org/charts.git"
projects[charts][download][tag] = "7.x-2.1"
projects[charts][subdir] = "contrib"

; CKEditor Link
; Base: 7.x-2.4
; Patches 17519b0, 495b5e2: patches from dev branch to fix hook implementation and allow internal links with spaces.
; Patch 9587716: Support arguments on internal links. https://www.drupal.org/project/ckeditor_link/issues/1491750#comment-11810261
; Patch fea94d31: Make extra argument guessing work properly for non-root sites. https://www.drupal.org/project/ckeditor_link/issues/1491750#comment-13794091
projects[ckeditor_link][type] = "module"
projects[ckeditor_link][download][type] = "git"
projects[ckeditor_link][download][url] = "https://git.uwaterloo.ca/drupal-org/ckeditor_link.git"
projects[ckeditor_link][download][tag] = "7.x-2.4-uw_wcms2"
projects[ckeditor_link][subdir] = "contrib"

; Coder
projects[coder][type] = "module"
projects[coder][download][type] = "git"
projects[coder][download][url] = "https://git.uwaterloo.ca/drupal-org/coder.git"
projects[coder][download][tag] = "7.x-2.6"
projects[coder][subdir] = "contrib"

; Colorbox
projects[colorbox][type] = "module"
projects[colorbox][download][type] = "git"
projects[colorbox][download][url] = "https://git.uwaterloo.ca/drupal-org/colorbox.git"
projects[colorbox][download][tag] = "7.x-2.17"
projects[colorbox][subdir] = "contrib"

; Composite Views Filter
projects[composite_views_filter][type] = "module"
projects[composite_views_filter][download][type] = "git"
projects[composite_views_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/composite_views_filter.git"
projects[composite_views_filter][download][tag] = "7.x-1.3"
projects[composite_views_filter][subdir] = "contrib"

; Conditional Fields
; Base: 7.x-3.0-alpha2
; Patch efd3840: Fix undefined index error on feature enable. https://www.drupal.org/node/2043563#comment-9049737
projects[conditional_fields][type] = "module"
projects[conditional_fields][download][type] = "git"
projects[conditional_fields][download][url] = "https://git.uwaterloo.ca/drupal-org/conditional_fields.git"
projects[conditional_fields][download][tag] = "7.x-3.0-alpha2-uw_wcms1"
projects[conditional_fields][subdir] = "contrib"

; Context
projects[context][type] = "module"
projects[context][download][type] = "git"
projects[context][download][url] = "https://git.uwaterloo.ca/drupal-org/context.git"
projects[context][download][tag] = "7.x-3.11"
projects[context][subdir] = "contrib"

; Context: Variable
projects[context_var][type] = "module"
projects[context_var][download][type] = "git"
projects[context_var][download][url] = "https://git.uwaterloo.ca/drupal-org/context_var.git"
projects[context_var][download][tag] = "7.x-1.1"
projects[context_var][subdir] = "contrib"

; Chaos tools
projects[ctools][type] = "module"
projects[ctools][download][type] = "git"
projects[ctools][download][url] = "https://git.uwaterloo.ca/drupal-org/ctools.git"
projects[ctools][download][tag] = "7.x-1.20"
projects[ctools][subdir] = "contrib"

; CTools Auto-modal
projects[ctools_automodal][type] = "module"
projects[ctools_automodal][download][type] = "git"
projects[ctools_automodal][download][url] = "https://git.uwaterloo.ca/drupal-org/ctools_automodal.git"
projects[ctools_automodal][download][tag] = "7.x-1.1"
projects[ctools_automodal][subdir] = "contrib"

; Custom Error
projects[customerror][type] = "module"
projects[customerror][download][type] = "git"
projects[customerror][download][url] = "https://git.uwaterloo.ca/drupal-org/customerror.git"
projects[customerror][download][tag] = "7.x-1.4"
projects[customerror][subdir] = "contrib"

; Date
; Base: 7.x-2.10
projects[date][type] = "module"
projects[date][download][type] = "git"
projects[date][download][url] = "https://git.uwaterloo.ca/drupal-org/date.git"
projects[date][download][tag] = "7.x-2.10-uw_wcms6"
projects[date][subdir] = "contrib"

; Date iCal
projects[date_ical][type] = "module"
projects[date_ical][download][type] = "git"
projects[date_ical][download][url] = "https://git.uwaterloo.ca/drupal-org/date_ical.git"
projects[date_ical][download][tag] = "7.x-3.10"
projects[date_ical][subdir] = "contrib"

; Default Menu Link
projects[default_menu_link][type] = "module"
projects[default_menu_link][download][type] = "git"
projects[default_menu_link][download][url] = "https://git.uwaterloo.ca/drupal-org/default_menu_link.git"
projects[default_menu_link][download][tag] = "7.x-1.3"
projects[default_menu_link][subdir] = "contrib"

; Devel
projects[devel][type] = "module"
projects[devel][download][type] = "git"
projects[devel][download][url] = "https://git.uwaterloo.ca/drupal-org/devel.git"
projects[devel][download][tag] = "7.x-1.7"
projects[devel][subdir] = "contrib"

; Theme developer
projects[devel_themer][type] = "module"
projects[devel_themer][download][type] = "git"
projects[devel_themer][download][url] = "https://git.uwaterloo.ca/drupal-org/devel_themer.git"
projects[devel_themer][download][branch] = "7.x-1.x"
projects[devel_themer][download][revision] = "cf347e1"
projects[devel_themer][subdir] = "contrib"

; Diff
projects[diff][type] = "module"
projects[diff][download][type] = "git"
projects[diff][download][url] = "https://git.uwaterloo.ca/drupal-org/diff.git"
projects[diff][download][tag] = "7.x-3.4"
projects[diff][subdir] = "contrib"

; Disable messages
projects[disable_messages][type] = "module"
projects[disable_messages][download][type] = "git"
projects[disable_messages][download][url] = "https://git.uwaterloo.ca/drupal-org/disable_messages.git"
projects[disable_messages][download][tag] = "7.x-1.2"
projects[disable_messages][subdir] = "contrib"

; Disable Term Node Listings
; Base: 7.x-1.2
; Patch c15ade3: Move menu alter hook to last position, so it executes after other modules (and therefore always works). https://drupal.org/node/1936304#comment-8652541
projects[disable_term_node_listings][type] = "module"
projects[disable_term_node_listings][download][type] = "git"
projects[disable_term_node_listings][download][url] = "https://git.uwaterloo.ca/drupal-org/disable_term_node_listings.git"
projects[disable_term_node_listings][download][tag] = "7.x-1.2-uw_wcms"
projects[disable_term_node_listings][subdir] = "contrib"

; Draggableviews
projects[draggableviews][type] = "module"
projects[draggableviews][download][type] = "git"
projects[draggableviews][download][url] = "https://git.uwaterloo.ca/drupal-org/draggableviews.git"
projects[draggableviews][download][tag] = "7.x-2.1"
projects[draggableviews][subdir] = "contrib"

; Drupal Reset
projects[drupal_reset][type] = "module"
projects[drupal_reset][download][type] = "git"
projects[drupal_reset][download][url] = "https://git.uwaterloo.ca/drupal-org/drupal_reset.git"
projects[drupal_reset][download][tag] = "7.x-1.6"
projects[drupal_reset][subdir] = "contrib"

; EIM
; Base: 7.x-1.3
; Patch 2f90652: Avoid multiple error message boxes. https://drupal.org/comment/8220183#comment-8220183
; Patch 3e362a7: Disable insert module integration / required validation. No Drupal issue.
; Patch 12e8b2b: Error when other modules try to dynamically load fields. https://www.drupal.org/node/2180035#comment-9443645
; Patch e4262d6: Support multiupload imagefield widget. https://www.drupal.org/node/1936826#comment-8341581
projects[eim][type] = "module"
projects[eim][download][type] = "git"
projects[eim][download][url] = "https://git.uwaterloo.ca/drupal-org/eim.git"
projects[eim][download][tag] = "7.x-1.3-uw_wcms3"
projects[eim][subdir] = "contrib"

; Elysia Cron
projects[elysia_cron][type] = "module"
projects[elysia_cron][download][type] = "git"
projects[elysia_cron][download][url] = "https://git.uwaterloo.ca/drupal-org/elysia_cron.git"
projects[elysia_cron][download][tag] = "7.x-2.9"
projects[elysia_cron][subdir] = "contrib"

; Email
projects[email][type] = "module"
projects[email][download][type] = "git"
projects[email][download][url] = "https://git.uwaterloo.ca/drupal-org/email.git"
projects[email][download][tag] = "7.x-1.3"
projects[email][subdir] = "contrib"

; Entity API
; Base: 7.x-1.10
; Patch 62b533a: General error: 1364 Field 'plugin' doesn't have a default value. https://www.drupal.org/node/2829437#comment-11839684
projects[entity][type] = "module"
projects[entity][download][type] = "git"
projects[entity][download][url] = "https://git.uwaterloo.ca/drupal-org/entity.git"
projects[entity][download][tag] = "7.x-1.10-uw_wcms1"
projects[entity][subdir] = "contrib"

; Entity menu links [Publication]
; Base: 7.x-1.0-alpha3
; Patch 9b8514f: Prevent fatal error with RESTFul module menu links. see https://www.drupal.org/node/2222467
projects[entity_menu_links][type] = "module"
projects[entity_menu_links][download][type] = "git"
projects[entity_menu_links][download][url] = "https://git.uwaterloo.ca/drupal-org/entity_menu_links.git"
projects[entity_menu_links][download][tag] = "7.x-1.0-alpha3-uw_wcms1"
projects[entity_menu_links][subdir] = "contrib"

; Entity Translation
projects[entity_translation][type] = "module"
projects[entity_translation][download][type] = "git"
projects[entity_translation][download][url] = "https://git.uwaterloo.ca/drupal-org/entity_translation.git"
projects[entity_translation][download][tag] = "7.x-1.1"
projects[entity_translation][subdir] = "contrib"

; Entity cache [Publication]
projects[entitycache][type] = "module"
projects[entitycache][download][type] = "git"
projects[entitycache][download][url] = "https://git.uwaterloo.ca/drupal-org/entitycache.git"
projects[entitycache][download][tag] = "7.x-1.5"
projects[entitycache][subdir] = "contrib"

; Entity Reference
projects[entityreference][type] = "module"
projects[entityreference][download][type] = "git"
projects[entityreference][download][url] = "https://git.uwaterloo.ca/drupal-org/entityreference.git"
projects[entityreference][download][tag] = "7.x-1.5"
projects[entityreference][subdir] = "contrib"

; EU Cookie Compliance
projects[eu_cookie_compliance][type] = "module"
projects[eu_cookie_compliance][download][type] = "git"
projects[eu_cookie_compliance][download][url] = "https://git.uwaterloo.ca/drupal-org/eu_cookie_compliance.git"
projects[eu_cookie_compliance][download][tag] = "7.x-1.43-uw_wcms1"
projects[eu_cookie_compliance][subdir] = "contrib"

; Expire
projects[expire][type] = "module"
projects[expire][download][type] = "git"
projects[expire][download][url] = "https://git.uwaterloo.ca/drupal-org/expire.git"
projects[expire][download][branch] = "7.x-2.x"
projects[expire][download][revision] = "f724c07"
projects[expire][subdir] = "contrib"

; Fast 404
projects[fast_404][type] = "module"
projects[fast_404][download][type] = "git"
projects[fast_404][download][url] = "https://git.uwaterloo.ca/drupal-org/fast_404.git"
projects[fast_404][download][tag] = "7.x-1.5"
projects[fast_404][subdir] = "contrib"

; Fast dblog
projects[fast_dblog][type] = "module"
projects[fast_dblog][download][type] = "git"
projects[fast_dblog][download][url] = "https://git.uwaterloo.ca/drupal-org/fast_dblog.git"
projects[fast_dblog][download][tag] = "7.x-1.1"
projects[fast_dblog][subdir] = "contrib"

; Features
; Base 7.x-2.14
; Patch ea4164d: Give site maintainers the option to disable content types. https://www.drupal.org/node/2855810#comment-11962085
projects[features][type] = "module"
projects[features][download][type] = "git"
projects[features][download][url] = "https://git.uwaterloo.ca/drupal-org/features.git"
projects[features][download][tag] = "7.x-2.14-uw_wcms1"
projects[features][subdir] = "contrib"

; Features Override
projects[features_override][type] = "module"
projects[features_override][download][type] = "git"
projects[features_override][download][url] = "https://git.uwaterloo.ca/drupal-org/features_override.git"
projects[features_override][download][tag] = "7.x-2.0-rc3"
projects[features_override][subdir] = "contrib"

; Feeds
projects[feeds][type] = "module"
projects[feeds][download][type] = "git"
projects[feeds][download][url] = "https://git.uwaterloo.ca/drupal-org/feeds.git"
projects[feeds][download][tag] = "7.x-2.0-beta6"
projects[feeds][subdir] = "contrib"

; Feeds XPath Parser
projects[feeds_xpathparser][type] = "module"
projects[feeds_xpathparser][download][type] = "git"
projects[feeds_xpathparser][download][url] = "https://git.uwaterloo.ca/drupal-org/feeds_xpathparser.git"
projects[feeds_xpathparser][download][tag] = "7.x-1.1"
projects[feeds_xpathparser][subdir] = "contrib"

; Fences
projects[fences][type] = "module"
projects[fences][download][type] = "git"
projects[fences][download][url] = "https://git.uwaterloo.ca/drupal-org/fences.git"
projects[fences][download][tag] = "7.x-1.2"
projects[fences][subdir] = "contrib"

; Field Collection
projects[field_collection][type] = "module"
projects[field_collection][download][type] = "git"
projects[field_collection][download][url] = "https://git.uwaterloo.ca/drupal-org/field_collection.git"
projects[field_collection][download][tag] = "7.x-1.2"
projects[field_collection][subdir] = "contrib"

; Fieldgroup
projects[field_group][type] = "module"
projects[field_group][download][type] = "git"
projects[field_group][download][url] = "https://git.uwaterloo.ca/drupal-org/field_group.git"
projects[field_group][download][tag] = "7.x-1.8"
projects[field_group][subdir] = "contrib"

; Field Permissions
projects[field_permissions][type] = "module"
projects[field_permissions][download][type] = "git"
projects[field_permissions][download][url] = "https://git.uwaterloo.ca/drupal-org/field_permissions.git"
projects[field_permissions][download][tag] = "7.x-1.1"
projects[field_permissions][subdir] = "contrib"

; Filefield Sources
projects[filefield_sources][type] = "module"
projects[filefield_sources][download][type] = "git"
projects[filefield_sources][download][url] = "https://git.uwaterloo.ca/drupal-org/filefield_sources.git"
projects[filefield_sources][download][tag] = "7.x-1.11"
projects[filefield_sources][subdir] = "contrib"

; Assetbank Filefield Sources
projects[filefield_sources_assetbank][type] = "module"
projects[filefield_sources_assetbank][download][type] = "git"
projects[filefield_sources_assetbank][download][url] = "https://git.uwaterloo.ca/drupal-org/filefield_sources_assetbank.git"
projects[filefield_sources_assetbank][download][tag] = "7.x-1.0"
projects[filefield_sources_assetbank][subdir] = "contrib"

; Fill PDF
projects[fillpdf][type] = "module"
projects[fillpdf][download][type] = "git"
projects[fillpdf][download][url] = "https://git.uwaterloo.ca/drupal-org/fillpdf.git"
projects[fillpdf][download][tag] = "7.x-1.18"
projects[fillpdf][subdir] = "contrib"

; Focal Point
; Base: 7.x-1.2
; Patch 34136e8: Added settings per fields vs. enabled for all image fields. Related to: https://www.drupal.org/node/2480947#comment-12188785
projects[focal_point][type] = "module"
projects[focal_point][download][type] = "git"
projects[focal_point][download][url] = "https://git.uwaterloo.ca/drupal-org/focal_point.git"
projects[focal_point][download][tag] = "7.x-1.2-uw_wcms1"
projects[focal_point][subdir] = "contrib"

; Forward
projects[forward][type] = "module"
projects[forward][download][type] = "git"
projects[forward][download][url] = "https://git.uwaterloo.ca/drupal-org/forward.git"
projects[forward][download][tag] = "7.x-2.1"
projects[forward][subdir] = "contrib"

; GMap
; Base: 7.x-2.11
; Patch c7115f9d: fix cache clear error. https://www.drupal.org/project/gmap/issues/2693297#comment-12602952
; Patch d9a6ba6b: fix compatibility with PHP 7.4. https://www.drupal.org/project/gmap/issues/3118279#comment-13926946
projects[gmap][type] = "module"
projects[gmap][download][type] = "git"
projects[gmap][download][url] = "https://git.uwaterloo.ca/drupal-org/gmap.git"
projects[gmap][download][tag] = "7.x-2.11-uw_wcms2"
projects[gmap][subdir] = "contrib"

; Google Tag Manager
projects[google_tag][type] = "module"
projects[google_tag][download][type] = "git"
projects[google_tag][download][url] = "https://git.uwaterloo.ca/drupal-org/google_tag.git"
projects[google_tag][download][tag] = "7.x-1.6"
projects[google_tag][subdir] = "contrib"

; Honeypot
projects[honeypot][type] = "module"
projects[honeypot][download][type] = "git"
projects[honeypot][download][url] = "https://git.uwaterloo.ca/drupal-org/honeypot.git"
projects[honeypot][download][tag] = "7.x-1.26"
projects[honeypot][subdir] = "contrib"

; Internationalization
projects[i18n][type] = "module"
projects[i18n][download][type] = "git"
projects[i18n][download][url] = "https://git.uwaterloo.ca/drupal-org/i18n.git"
projects[i18n][download][tag] = "7.x-1.31"
projects[i18n][subdir] = "contrib"

; ImageAPI Optimize
projects[imageapi_optimize][type] = "module"
projects[imageapi_optimize][download][type] = "git"
projects[imageapi_optimize][download][url] = "https://git.uwaterloo.ca/drupal-org/imageapi_optimize.git"
projects[imageapi_optimize][download][tag] = "7.x-1.3"
projects[imageapi_optimize][subdir] = "contrib"

; Image Field Caption
; Base: 7.x-2.3
; Patch 244835cb: Fix notice (PHP 7.4 compatibility?). https://www.drupal.org/project/image_field_caption/issues/3200418#comment-14011597
projects[image_field_caption][type] = "module"
projects[image_field_caption][download][type] = "git"
projects[image_field_caption][download][url] = "https://git.uwaterloo.ca/drupal-org/image_field_caption.git"
projects[image_field_caption][download][tag] = "7.x-2.3-uw_wcms1"
projects[image_field_caption][subdir] = "contrib"

; ImageMagick
; Base: 7.x-1.0
; Patch 443cdf11: Fix Animated GIF resizing. https://www.drupal.org/node/1802534#comment-9275011.
projects[imagemagick][type] = "module"
projects[imagemagick][download][type] = "git"
projects[imagemagick][download][url] = "https://git.uwaterloo.ca/drupal-org/imagemagick.git"
projects[imagemagick][download][tag] = "7.x-1.0-uw_wcms1"
projects[imagemagick][subdir] = "contrib"

; Image Resize Filter
projects[image_resize_filter][type] = "module"
projects[image_resize_filter][download][type] = "git"
projects[image_resize_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/image_resize_filter.git"
projects[image_resize_filter][download][tag] = "7.x-1.16"
projects[image_resize_filter][subdir] = "contrib"

; IMCE
projects[imce][type] = "module"
projects[imce][download][type] = "git"
projects[imce][download][url] = "https://git.uwaterloo.ca/drupal-org/imce.git"
projects[imce][download][tag] = "7.x-1.11"
projects[imce][subdir] = "contrib"

; Insert
projects[insert][type] = "module"
projects[insert][download][type] = "git"
projects[insert][download][url] = "https://git.uwaterloo.ca/drupal-org/insert.git"
projects[insert][download][tag] = "7.x-1.4"
projects[insert][subdir] = "contrib"

; Job Scheduler
projects[job_scheduler][type] = "module"
projects[job_scheduler][download][type] = "git"
projects[job_scheduler][download][url] = "https://git.uwaterloo.ca/drupal-org/job_scheduler.git"
projects[job_scheduler][download][tag] = "7.x-2.0"
projects[job_scheduler][subdir] = "contrib"

; Jquery Colorpicker
projects[jquery_colorpicker][type] = "module"
projects[jquery_colorpicker][download][type] = "git"
projects[jquery_colorpicker][download][url] = "https://git.uwaterloo.ca/drupal-org/jquery_colorpicker.git"
projects[jquery_colorpicker][download][tag] = "7.x-1.3"
projects[jquery_colorpicker][subdir] = "contrib"

; jQuery Update
projects[jquery_update][type] = "module"
projects[jquery_update][download][type] = "git"
projects[jquery_update][download][url] = "https://git.uwaterloo.ca/drupal-org/jquery_update.git"
projects[jquery_update][download][tag] = "7.x-2.7"
projects[jquery_update][subdir] = "contrib"

; Label Help
projects[label_help][type] = "module"
projects[label_help][download][type] = "git"
projects[label_help][download][url] = "https://git.uwaterloo.ca/drupal-org/label_help.git"
projects[label_help][download][tag] = "7.x-1.2"
projects[label_help][subdir] = "contrib"

; Leaflet
; Base: 7.x-1.4
; Patch 00108b3: ISTWCMS-2636/Issue #2995678: Show address with map. https://www.drupal.org/project/leaflet/issues/2995678#comment-12753198
; Patch 03a7694: ISTWCMS-2636/ISTWCMS-2739/Issue #2062873: Integrate with location module. https://www.drupal.org/project/leaflet/issues/2062873#comment-12848418
; Patch 4fe79b7: ISTWCMS-2636/Issue #2455853: Should popup use a textarea instead of textfield. https://www.drupal.org/project/leaflet/issues/2455853#comment-9738831
; Patch 737c04d: ISTWCMS-2636: Ensuring that the address settings get saved.
projects[leaflet][type] = "module"
projects[leaflet][download][type] = "git"
projects[leaflet][download][url] = "https://git.uwaterloo.ca/drupal-org/leaflet.git"
projects[leaflet][download][tag] = "7.x-1.4-uw_wcms1"
projects[leaflet][subdir] = "contrib"

; Libraries
projects[libraries][type] = "module"
projects[libraries][download][type] = "git"
projects[libraries][download][url] = "https://git.uwaterloo.ca/drupal-org/libraries.git"
projects[libraries][download][tag] = "7.x-2.5"
projects[libraries][subdir] = "contrib"

; Link
projects[link][type] = "module"
projects[link][download][type] = "git"
projects[link][download][url] = "https://git.uwaterloo.ca/drupal-org/link.git"
projects[link][download][tag] = "7.x-1.11"
projects[link][subdir] = "contrib"

; Linkchecker
projects[linkchecker][type] = "module"
projects[linkchecker][download][type] = "git"
projects[linkchecker][download][url] = "https://git.uwaterloo.ca/drupal-org/linkchecker.git"
projects[linkchecker][download][tag] = "7.x-1.4"
projects[linkchecker][subdir] = "contrib"

; Location
projects[location][type] = "module"
projects[location][download][type] = "git"
projects[location][download][url] = "https://git.uwaterloo.ca/drupal-org/location.git"
projects[location][download][tag] = "7.x-3.7"
projects[location][subdir] = "contrib"

; Location Feeds
; Base: 7.x-1.6
; Patch 508b1f1: Issue #2367135: Fix inclusion of test to use correct filename from location module. https://www.drupal.org/node/2367135#comment-9569629
projects[location_feeds][type] = "module"
projects[location_feeds][download][type] = "git"
projects[location_feeds][download][url] = "https://git.uwaterloo.ca/drupal-org/location_feeds.git"
projects[location_feeds][download][tag] = "7.x-1.6-uw_wcms1"
projects[location_feeds][subdir] = "contrib"

; mailsystem
projects[mailsystem][type] = "module"
projects[mailsystem][download][type] = "git"
projects[mailsystem][download][url] = "https://git.uwaterloo.ca/drupal-org/mailsystem.git"
projects[mailsystem][download][tag] = "7.x-2.35"
projects[mailsystem][subdir] = "contrib"

; Markup
projects[markup][type] = "module"
projects[markup][download][type] = "git"
projects[markup][download][url] = "https://git.uwaterloo.ca/drupal-org/markup.git"
projects[markup][download][tag] = "7.x-1.3"
projects[markup][subdir] = "contrib"

; MathJax
projects[mathjax][type] = "module"
projects[mathjax][download][type] = "git"
projects[mathjax][download][url] = "https://git.uwaterloo.ca/drupal-org/mathjax.git"
projects[mathjax][download][tag] = "7.x-2.5"
projects[mathjax][subdir] = "contrib"

; More Buttons
; Base: cf380cf (7.x-1.0-beta1+5-dev)
; Patch 8963d49: Prevent notice: Trying to get property of non-object in mb_content_menu_local_tasks_alter(). https://drupal.org/node/1526178
; Patch a1754dd: Issue #2577143: Fix Notice: Undefined index: #id in mb_content_changed_validate(). https://www.drupal.org/node/2577143#comment-10390005
; Patch 670fab8: Fix undefined variable notices. https://www.drupal.org/node/2860587#comment-11986273
projects[mb][type] = "module"
projects[mb][download][type] = "git"
projects[mb][download][url] = "https://git.uwaterloo.ca/drupal-org/mb.git"
projects[mb][download][tag] = "7.x-1.0-beta1+5-dev-uw_wcms3"
projects[mb][subdir] = "contrib"

; Memcache
projects[memcache][type] = "module"
projects[memcache][download][type] = "git"
projects[memcache][download][url] = "https://git.uwaterloo.ca/drupal-org/memcache.git"
projects[memcache][download][tag] = "7.x-1.8"
projects[memcache][subdir] = "contrib"

; Menu Admin per Menu
projects[menu_admin_per_menu][type] = "module"
projects[menu_admin_per_menu][download][type] = "git"
projects[menu_admin_per_menu][download][url] = "https://git.uwaterloo.ca/drupal-org/menu_admin_per_menu.git"
projects[menu_admin_per_menu][download][tag] = "7.x-1.1"
projects[menu_admin_per_menu][subdir] = "contrib"

; Metatag
projects[metatag][type] = "module"
projects[metatag][download][type] = "git"
projects[metatag][download][url] = "https://git.uwaterloo.ca/drupal-org/metatag.git"
projects[metatag][download][tag] = "7.x-1.30"
projects[metatag][subdir] = "contrib"

; mimemail
projects[mimemail][type] = "module"
projects[mimemail][download][type] = "git"
projects[mimemail][download][url] = "https://git.uwaterloo.ca/drupal-org/mimemail.git"
projects[mimemail][download][tag] = "7.x-1.2"
projects[mimemail][subdir] = "contrib"

; Missing module
projects[missing_module][type] = "module"
projects[missing_module][download][type] = "git"
projects[missing_module][download][url] = "https://git.uwaterloo.ca/drupal-org/missing_module.git"
projects[missing_module][download][tag] = "7.x-1.2"
projects[missing_module][subdir] = "contrib"

; Module filter
projects[module_filter][type] = "module"
projects[module_filter][download][type] = "git"
projects[module_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/module_filter.git"
projects[module_filter][download][tag] = "7.x-2.2"
projects[module_filter][subdir] = "contrib"

; Multiupload File Field Widget
; Base: 7.x-1.13
; Patch b8277f4: Make compatible with FileField Sources module. https://www.drupal.org/node/1808540#comment-8483585
; Patch 3d1fe7d: Support 'accept' attribute to limit files to supported types. https://www.drupal.org/node/2510316#comment-10048080
projects[multiupload_filefield_widget][type] = "module"
projects[multiupload_filefield_widget][download][type] = "git"
projects[multiupload_filefield_widget][download][url] = "https://git.uwaterloo.ca/drupal-org/multiupload_filefield_widget.git"
projects[multiupload_filefield_widget][download][tag] = "7.x-1.13-uw_wcms1"
projects[multiupload_filefield_widget][subdir] = "contrib"

; Multiupload Image Field Widget
; Base: 7.x-1.3
; Patch 37e6839: Make compatible with FileField Sources module. https://www.drupal.org/node/1412636#comment-10739614
projects[multiupload_imagefield_widget][type] = "module"
projects[multiupload_imagefield_widget][download][type] = "git"
projects[multiupload_imagefield_widget][download][url] = "https://git.uwaterloo.ca/drupal-org/multiupload_imagefield_widget.git"
projects[multiupload_imagefield_widget][download][tag] = "7.x-1.3-uw_wcms1"
projects[multiupload_imagefield_widget][subdir] = "contrib"

; NavBar
; Base: 7.x-1.8
; Patch bfb0e44: Issue #2644930: Navbar link language render to user preference. https://www.drupal.org/node/2644930#comment-10720730
; Patch 79fddad: Issue #2918703: Remove invalid attribute aria-owned-by. https://www.drupal.org/project/navbar/issues/2918703#comment-12314761
projects[navbar][type] = "module"
projects[navbar][download][type] = "git"
projects[navbar][download][url] = "https://git.uwaterloo.ca/drupal-org/navbar.git"
projects[navbar][download][tag] = "7.x-1.8-uw_wcms1"
projects[navbar][subdir] = "contrib"

; Node clone
; Base: 7.x-1.0
; Patch 074f32f: Issue #2460445: "Clone of" prefix does not appear in title fields converted by the Title module. https://www.drupal.org/node/2460445#comment-9764243
; Patch 9c73d71: Issue #2501357: Reset Workbench Moderation state. https://www.drupal.org/project/node_clone/issues/2501357#comment-12771501
projects[node_clone][type] = "module"
projects[node_clone][download][type] = "git"
projects[node_clone][download][url] = "https://git.uwaterloo.ca/drupal-org/node_clone.git"
projects[node_clone][download][tag] = "7.x-1.0-uw_wcms2"
projects[node_clone][subdir] = "contrib"

; Node clone tab
projects[node_clone_tab][type] = "module"
projects[node_clone_tab][download][type] = "git"
projects[node_clone_tab][download][url] = "https://git.uwaterloo.ca/drupal-org/node_clone_tab.git"
projects[node_clone_tab][download][tag] = "7.x-1.1"
projects[node_clone_tab][subdir] = "contrib"

; Node export
; Base: 7.x-3.1
; Patch fcb7871: PHP 7.2+ compatibility. https://www.drupal.org/project/node_export/issues/2946171#comment-12518654
projects[node_export][type] = "module"
projects[node_export][download][type] = "git"
projects[node_export][download][url] = "https://git.uwaterloo.ca/drupal-org/node_export.git"
projects[node_export][download][tag] = "7.x-3.1-uw_wcms1"
projects[node_export][subdir] = "contrib"

; Node revision delete
projects[node_revision_delete][type] = "module"
projects[node_revision_delete][download][type] = "git"
projects[node_revision_delete][download][url] = "https://git.uwaterloo.ca/drupal-org/node_revision_delete.git"
projects[node_revision_delete][download][tag] = "7.x-3.2"
projects[node_revision_delete][subdir] = "contrib"

; Office hours
; Base: 7.x-1.10
; Patch fc5f9cb: Issue #3167972: Fix invalid HTML in theme_office_hours_field_formatter_default(). https://www.drupal.org/project/offc5f9cbaefc5f9cbaefice_hours/issues/3167972#comment-13804546
projects[office_hours][type] = "module"
projects[office_hours][download][type] = "git"
projects[office_hours][download][url] = "https://git.uwaterloo.ca/drupal-org/office_hours.git"
projects[office_hours][download][tag] = "7.x-1.10-uw_wcms1"
projects[office_hours][subdir] = "contrib"

; Options element
projects[options_element][type] = "module"
projects[options_element][download][type] = "git"
projects[options_element][download][url] = "https://git.uwaterloo.ca/drupal-org/options_element.git"
projects[options_element][download][tag] = "7.x-1.12"
projects[options_element][subdir] = "contrib"

; Override node options
projects[override_node_options][type] = "module"
projects[override_node_options][download][type] = "git"
projects[override_node_options][download][url] = "https://git.uwaterloo.ca/drupal-org/override_node_options.git"
projects[override_node_options][download][tag] = "7.x-1.14"
projects[override_node_options][subdir] = "contrib"

; Page Title
; Base: 7.x-2.7
; Patch 631c02a: Ensure Characters Entered text is not duplicated on node add. https://drupal.org/comment/4822042#comment-4822042
; NOTE: althrough this module is deprecated and replaced by Metatag, which we also run, DO NOT DISABLE OR REMOVE.
; We use functionality that Metatag does not currently provide. See https://www.drupal.org/node/2539444#comment-11854767
projects[page_title][type] = "module"
projects[page_title][download][type] = "git"
projects[page_title][download][url] = "https://git.uwaterloo.ca/drupal-org/page_title.git"
projects[page_title][download][tag] = "7.x-2.7-uw_wcms"
projects[page_title][subdir] = "contrib"

; Paragraphs
; Base: 7.x-1.0-rc5
; Patch eaf6a59: Fix broken images in nested Paragraph when using Workbench Moderation. https://www.drupal.org/node/2603424#comment-11442413
; Patch 6035b5c: Add validation for required paragraph field. https://www.drupal.org/project/paragraphs/issues/2558517#comment-12385180
; Patch 2740e03: Paragraphs data loss with Node Clone module. https://www.drupal.org/project/paragraphs/issues/2394313#comment-12590359
; Patch 0617b00: Add paragraphs bundle class to row in backend - Fixes and Updates for PHP7. https://www.drupal.org/project/paragraphs/issues/3010369#comment-12846591
; Patch caebf6c: cherry-pick 8cc0f95 - Issue #3010938 by jacob.embree, sickness29: [PHP 7.2] count() on non-countable
; Patch d69c1ec1: paragraphs AJAX buttons do nothing if drupal_html_id() has been called early. https://www.drupal.org/project/paragraphs/issues/2680101#comment-10923457
projects[paragraphs][type] = "module"
projects[paragraphs][download][type] = "git"
projects[paragraphs][download][url] = "https://git.uwaterloo.ca/drupal-org/paragraphs.git"
projects[paragraphs][download][tag] = "7.x-1.0-rc5-uw_wcms8"
projects[paragraphs][subdir] = "contrib"

; Pathauto
; Note: Workbench moderation (7.x-1.4-uw_wcms3) is patched in order to allow version 7.x-1.3.
projects[pathauto][type] = "module"
projects[pathauto][download][type] = "git"
projects[pathauto][download][url] = "https://git.uwaterloo.ca/drupal-org/pathauto.git"
projects[pathauto][download][tag] = "7.x-1.3"
projects[pathauto][subdir] = "contrib"

; Postal Code Validation
projects[postal_code_validation][type] = "module"
projects[postal_code_validation][download][type] = "git"
projects[postal_code_validation][download][url] = "https://git.uwaterloo.ca/drupal-org/postal_code_validation.git"
projects[postal_code_validation][download][tag] = "7.x-1.7"
projects[postal_code_validation][subdir] = "contrib"

; Redirect 403 to User Login
projects[r4032login][type] = "module"
projects[r4032login][download][type] = "git"
projects[r4032login][download][url] = "https://git.uwaterloo.ca/drupal-org/r4032login.git"
projects[r4032login][download][tag] = "7.x-1.8"
projects[r4032login][subdir] = "contrib"

; Real name
projects[realname][type] = "module"
projects[realname][download][type] = "git"
projects[realname][download][url] = "https://git.uwaterloo.ca/drupal-org/realname.git"
projects[realname][download][tag] = "7.x-1.4"
projects[realname][subdir] = "contrib"

; reCAPTCHA
projects[recaptcha][type] = "module"
projects[recaptcha][download][type] = "git"
projects[recaptcha][download][url] = "https://git.uwaterloo.ca/drupal-org/recaptcha.git"
projects[recaptcha][download][tag] = "7.x-2.3"
projects[recaptcha][subdir] = "contrib"

; Redirect
; Base: 7.x-1.0-rc4
; Patch 726facc: Issue #2327247: Validate internal URLs so redirects cannot lead to 404 pages. https://www.drupal.org/project/redirect/issues/2327247#comment-13063973
; Patch bfc1f68: Issue #905914: Merge global redirect functions into Redirect module. https://www.drupal.org/project/redirect/issues/905914#comment-13846444
projects[redirect][type] = "module"
projects[redirect][download][type] = "git"
projects[redirect][download][url] = "https://git.uwaterloo.ca/drupal-org/redirect.git"
projects[redirect][download][tag] = "7.x-1.0-rc4-uw_wcms1"
projects[redirect][subdir] = "contrib"

; References
projects[references][type] = "module"
projects[references][download][type] = "git"
projects[references][download][url] = "https://git.uwaterloo.ca/drupal-org/references.git"
projects[references][download][tag] = "7.x-2.4"
projects[references][subdir] = "contrib"

; Responsive Menu Combined
; This contrib module was written by Tyler, so in our dev branch of the profile we point to the dev branch of the module.
projects[responsive_menu_combined][type] = "module"
projects[responsive_menu_combined][download][type] = "git"
projects[responsive_menu_combined][download][url] = "https://git.uwaterloo.ca/drupal-org/responsive_menu_combined.git"
projects[responsive_menu_combined][download][branch] = "7.x-1.x"
projects[responsive_menu_combined][download][revision] = "e432a0f"
projects[responsive_menu_combined][subdir] = "contrib"

; Responsive Tables Filter
projects[responsive_tables_filter][type] = "module"
projects[responsive_tables_filter][download][type] = "git"
projects[responsive_tables_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/responsive_tables_filter.git"
projects[responsive_tables_filter][download][branch] = "7.x-1.x"
projects[responsive_tables_filter][download][revision] = "f225b21"
projects[responsive_tables_filter][subdir] = "contrib"

; Restful
projects[restful][type] = "module"
projects[restful][download][type] = "git"
projects[restful][download][url] = "https://git.uwaterloo.ca/drupal-org/restful.git"
projects[restful][download][tag] = "7.x-1.10"
projects[restful][subdir] = "contrib"

; Reuse Cached 404s
projects[reuse_cached_404][type] = "module"
projects[reuse_cached_404][download][type] = "git"
projects[reuse_cached_404][download][url] = "https://git.uwaterloo.ca/drupal-org/reuse_cached_404.git"
projects[reuse_cached_404][download][tag] = "7.x-1.1"
projects[reuse_cached_404][subdir] = "contrib"

; Role Expire
; Base: 7.x-1.2
; Patch 490b0b3: Should only be able to expire roles that a user can assign. https://www.drupal.org/project/role_expire/issues/3154642#comment-13717462
projects[role_expire][type] = "module"
projects[role_expire][download][type] = "git"
projects[role_expire][download][url] = "https://git.uwaterloo.ca/drupal-org/role_expire.git"
projects[role_expire][download][tag] = "7.x-1.2-uw_wcms1"
projects[role_expire][subdir] = "contrib"

; RoleAssign
projects[roleassign][type] = "module"
projects[roleassign][download][type] = "git"
projects[roleassign][download][url] = "https://git.uwaterloo.ca/drupal-org/roleassign.git"
projects[roleassign][download][tag] = "7.x-1.2"
projects[roleassign][subdir] = "contrib"

; Rules
projects[rules][type] = "module"
projects[rules][download][type] = "git"
projects[rules][download][url] = "https://git.uwaterloo.ca/drupal-org/rules.git"
projects[rules][download][tag] = "7.x-2.13"
projects[rules][subdir] = "contrib"

; Rules Condition Site Variable
projects[rules_condition_site_variable][type] = "module"
projects[rules_condition_site_variable][download][type] = "git"
projects[rules_condition_site_variable][download][url] = "https://git.uwaterloo.ca/drupal-org/rules_condition_site_variable.git"
projects[rules_condition_site_variable][download][tag] = "7.x-1.1"
projects[rules_condition_site_variable][subdir] = "contrib"

; Rules for Teams
projects[rules_teams][type] = "module"
projects[rules_teams][download][type] = "git"
projects[rules_teams][download][url] = "https://git.uwaterloo.ca/drupal-org/rules_teams.git"
projects[rules_teams][download][tag] = "7.x-1.0-alpha1"
projects[rules_teams][subdir] = "contrib"

; Scheduler
; Base: 7.x-1.6
; Patch 53bb3e9: Issue #2397423: Publish and Unpublish as a specific user. https://www.drupal.org/node/2397423#comment-12000666
projects[scheduler][type] = "module"
projects[scheduler][download][type] = "git"
projects[scheduler][download][url] = "https://git.uwaterloo.ca/drupal-org/scheduler.git"
projects[scheduler][download][tag] = "7.x-1.6-uw_wcms1"
projects[scheduler][subdir] = "contrib"

; Scheduler Workbench Integration
; Base: 7.x-1.3
; Patch 08df209: Issue #2230539: Fixed Scheduler does not unplubish when using workbench moderation. https://www.drupal.org/node/2230539#comment-9553805
; Patch 6c4311f: FDSU-563. Added uw_wb_config dependency to allow setting of moderation permissions. No Drupal issue.
projects[scheduler_workbench][type] = "module"
projects[scheduler_workbench][download][type] = "git"
projects[scheduler_workbench][download][url] = "https://git.uwaterloo.ca/drupal-org/scheduler_workbench.git"
projects[scheduler_workbench][download][tag] = "7.x-1.3-uw_wcms1"
projects[scheduler_workbench][subdir] = "contrib"

; Schema
projects[schema][type] = "module"
projects[schema][download][type] = "git"
projects[schema][download][url] = "https://git.uwaterloo.ca/drupal-org/schema.git"
projects[schema][download][tag] = "7.x-1.3"
projects[schema][subdir] = "contrib"

; Schema.org
projects[schemaorg][type] = "module"
projects[schemaorg][download][type] = "git"
projects[schemaorg][download][url] = "https://git.uwaterloo.ca/drupal-org/schemaorg.git"
projects[schemaorg][download][tag] = "7.x-1.0-rc1"
projects[schemaorg][subdir] = "contrib"

; Search Configuration
projects[search_config][type] = "module"
projects[search_config][download][type] = "git"
projects[search_config][download][url] = "https://git.uwaterloo.ca/drupal-org/search_config.git"
projects[search_config][download][tag] = "7.x-1.1"
projects[search_config][subdir] = "contrib"

; Search Krumo
projects[search_krumo][type] = "module"
projects[search_krumo][download][type] = "git"
projects[search_krumo][download][url] = "https://git.uwaterloo.ca/drupal-org/search_krumo.git"
projects[search_krumo][download][tag] = "7.x-1.6"
projects[search_krumo][subdir] = "contrib"

; Select (or other)
projects[select_or_other][type] = "module"
projects[select_or_other][download][type] = "git"
projects[select_or_other][download][url] = "https://git.uwaterloo.ca/drupal-org/select_or_other.git"
projects[select_or_other][download][tag] = "7.x-2.24"
projects[select_or_other][subdir] = "contrib"

; Service links
; Base: 7.x-2.4
; Patch 2dc7f37: Improve HTML of service links in blocks. https://www.drupal.org/node/2072891#comment-7886391
projects[service_links][type] = "module"
projects[service_links][download][type] = "git"
projects[service_links][download][url] = "https://git.uwaterloo.ca/drupal-org/service_links.git"
projects[service_links][download][tag] = "7.x-2.4-uw_wcms1"
projects[service_links][subdir] = "contrib"

; Services
projects[services][type] = "module"
projects[services][download][type] = "git"
projects[services][download][url] = "https://git.uwaterloo.ca/drupal-org/services_.git"
projects[services][download][tag] = "7.x-3.28"
projects[services][subdir] = "contrib"

; Services API Key Authentication
projects[services_api_key_auth][type] = "module"
projects[services_api_key_auth][download][type] = "git"
projects[services_api_key_auth][download][url] = "https://git.uwaterloo.ca/drupal-org/services_api_key_auth.git"
projects[services_api_key_auth][download][tag] = "7.x-1.3"
projects[services_api_key_auth][subdir] = "contrib"

; Services Views
; Base: 2016541 (7.x-1.4)
; Patch fb0e9e9: RT#524581: Fix illegal string offset warnings all_(events|news|etc).json. https://www.drupal.org/node/2289431#comment-10647720
; Patch ac22715: RT#641950: Fix Location API return NULL. https://www.drupal.org/project/services_views/issues/2907068#comment-12248893
projects[services_views][type] = "module"
projects[services_views][download][type] = "git"
projects[services_views][download][url] = "https://git.uwaterloo.ca/drupal-org/services_views.git"
projects[services_views][download][tag] = "7.x-1.4-uw_wcms1"
projects[services_views][subdir] = "contrib"

; simplehtmldom API
projects[simplehtmldom][type] = "module"
projects[simplehtmldom][download][type] = "git"
projects[simplehtmldom][download][url] = "https://git.uwaterloo.ca/drupal-org/simplehtmldom.git"
projects[simplehtmldom][download][tag] = "7.x-1.12"
projects[simplehtmldom][subdir] = "contrib"

; simplesamlphp_auth
projects[simplesamlphp_auth[type] = "module"
projects[simplesamlphp_auth][download][type] = "git"
projects[simplesamlphp_auth][download][url] = "https://git.uwaterloo.ca/drupal-org/simplesamlphp_auth.git"
projects[simplesamlphp_auth][download][branch] = "7.x-3.x"
projects[simplesamlphp_auth][download][revision] = "9fadbc4"
projects[simplesamlphp_auth][subdir] = "contrib"

; Site Test
; Base: 7.x-1.5
; Patch 834bd58: Issue #2960815: Set verboseDirectoryUrl in setUp*(); this allows links to verbose output to work.
projects[site_test][type] = "module"
projects[site_test][download][type] = "git"
projects[site_test][download][url] = "https://git.uwaterloo.ca/drupal-org/site_test.git"
projects[site_test][download][tag] = "7.x-1.5-uw_wcms1"
projects[site_test][subdir] = "contrib"

; Slack
projects[slack][type] = "module"
projects[slack][download][type] = "git"
projects[slack][download][url] = "https://git.uwaterloo.ca/drupal-org/slack.git"
projects[slack][download][tag] = "7.x-1.7"
projects[slack][subdir] = "contrib"

; Sliderfield
projects[sliderfield][type] = "module"
projects[sliderfield][download][type] = "git"
projects[sliderfield][download][url] = "https://git.uwaterloo.ca/drupal-org/sliderfield.git"
projects[sliderfield][download][tag] = "7.x-2.1-beta1"
projects[sliderfield][subdir] = "contrib"

; Special Menu Items
projects[special_menu_items][type] = "module"
projects[special_menu_items][download][type] = "git"
projects[special_menu_items][download][url] = "https://git.uwaterloo.ca/drupal-org/special_menu_items.git"
projects[special_menu_items][download][tag] = "7.x-2.1"
projects[special_menu_items][subdir] = "contrib"

; Strongarm
projects[strongarm][type] = "module"
projects[strongarm][download][type] = "git"
projects[strongarm][download][url] = "https://git.uwaterloo.ca/drupal-org/strongarm.git"
projects[strongarm][download][tag] = "7.x-2.0"
projects[strongarm][subdir] = "contrib"

; Styleguide
projects[styleguide][type] = "module"
projects[styleguide][download][type] = "git"
projects[styleguide][download][url] = "https://git.uwaterloo.ca/drupal-org/styleguide.git"
projects[styleguide][download][tag] = "7.x-1.1"
projects[styleguide][subdir] = "contrib"

; SVG Image Tag
projects[svgimg][type] = "module"
projects[svgimg][download][type] = "git"
projects[svgimg][download][url] = "https://git.uwaterloo.ca/drupal-org/svgimg.git"
projects[svgimg][download][branch] = "7.x-1.x"
projects[svgimg][download][revision] = "bc64a048"
projects[svgimg][subdir] = "contrib"

; Tablefield
projects[tablefield][type] = "module"
projects[tablefield][download][type] = "git"
projects[tablefield][download][url] = "https://git.uwaterloo.ca/drupal-org/tablefield.git"
projects[tablefield][download][tag] = "7.x-3.6"
projects[tablefield][subdir] = "contrib"

; Tab Tamer
projects[tabtamer][type] = "module"
projects[tabtamer][download][type] = "git"
projects[tabtamer][download][url] = "https://git.uwaterloo.ca/drupal-org/tabtamer.git"
projects[tabtamer][download][tag] = "7.x-1.1"
projects[tabtamer][subdir] = "contrib"

; Taxonomy access fix
projects[taxonomy_access_fix][type] = "module"
projects[taxonomy_access_fix][download][type] = "git"
projects[taxonomy_access_fix][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_access_fix.git"
projects[taxonomy_access_fix][download][tag] = "7.x-2.4"
projects[taxonomy_access_fix][subdir] = "contrib"

; Taxonomy CSV import/export
projects[taxonomy_csv][type] = "module"
projects[taxonomy_csv][download][type] = "git"
projects[taxonomy_csv][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_csv.git"
projects[taxonomy_csv][download][tag] = "7.x-5.11"
projects[taxonomy_csv][subdir] = "contrib"

; Taxonomy display
projects[taxonomy_display][type] = "module"
projects[taxonomy_display][download][type] = "git"
projects[taxonomy_display][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_display.git"
projects[taxonomy_display][download][tag] = "7.x-1.1"
projects[taxonomy_display][subdir] = "contrib"

; Taxonomy Formatter
projects[taxonomy_formatter][type] = "module"
projects[taxonomy_formatter][download][type] = "git"
projects[taxonomy_formatter][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_formatter.git"
projects[taxonomy_formatter][download][tag] = "7.x-1.4"
projects[taxonomy_formatter][subdir] = "contrib"

; Taxonomy Machine Name [Publication]
projects[taxonomy_machine_name][type] = "module"
projects[taxonomy_machine_name][download][type] = "git"
projects[taxonomy_machine_name][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_machine_name.git"
projects[taxonomy_machine_name][download][tag] = "7.x-1.2"
projects[taxonomy_machine_name][subdir] = "contrib"

; Taxonomy menu form [Publication]
; Base: 7.x-1.1
; Patch 2bb81fd: Custom UWaterloo patch to use unique permission instead of "administer menu".
; Patch ed3928e: Set weight of the module to -1 to allow it to work iwth Menu Admin per Menu module.
projects[taxonomy_menu_form][type] = "module"
projects[taxonomy_menu_form][download][type] = "git"
projects[taxonomy_menu_form][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_menu_form.git"
projects[taxonomy_menu_form][download][tag] = "7.x-1.1-uw_wcms2"
projects[taxonomy_menu_form][subdir] = "contrib"

; Taxonomy Orphanage
projects[taxonomy_orphanage][type] = "module"
projects[taxonomy_orphanage][download][type] = "git"
projects[taxonomy_orphanage][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_orphanage.git"
projects[taxonomy_orphanage][download][tag] = "7.x-1.2"
projects[taxonomy_orphanage][subdir] = "contrib"

; Term Merge
; Base: 7.x-1.4
; Patch cfaff8d: Improve some module text. https://www.drupal.org/project/term_merge/issues/2948523#comment-12502000
; Patch 342bb7f: Go to merge page on merge completion. https://www.drupal.org/project/term_merge/issues/2948524#comment-12502014
; Patch 753c364: Make auto-switch-to-autocomplete threshhold user-configurable. https://www.drupal.org/project/term_merge/issues/2948784#comment-12503429
projects[term_merge][type] = "module"
projects[term_merge][download][type] = "git"
projects[term_merge][download][url] = "https://git.uwaterloo.ca/drupal-org/term_merge.git"
projects[term_merge][download][tag] = "7.x-1.4-uw_wcms1"
projects[term_merge][subdir] = "contrib"

; Term Reference Tree
; Base: 7.x-1.11
; Patch be7820f: Use current weights and terms in display formatter. https://www.drupal.org/project/term_reference_tree/issues/1927252#comment-12475419
; Patch 441a95c: Provide "use inline display" option. No Drupal issue.
; Patch f5e0766: Removed bold styling introduced in latest version. No Drupal issue.
; Patch 776c1a9: i18n compatibility. https://www.drupal.org/node/1514794#comment-7533593
; Patch b516cc0: Parameter must be an array or an object that implements Countable. Cherry-pick from 2a75e05.
projects[term_reference_tree][type] = "module"
projects[term_reference_tree][download][type] = "git"
projects[term_reference_tree][download][url] = "https://git.uwaterloo.ca/drupal-org/term_reference_tree.git"
projects[term_reference_tree][download][tag] = "7.x-1.11-uw_wcms2"
projects[term_reference_tree][subdir] = "contrib"

; Title
projects[title][type] = "module"
projects[title][download][type] = "git"
projects[title][download][url] = "https://git.uwaterloo.ca/drupal-org/title.git"
projects[title][download][tag] = "7.x-1.0-beta4"
projects[title][subdir] = "contrib"

; Token
projects[token][type] = "module"
projects[token][download][type] = "git"
projects[token][download][url] = "https://git.uwaterloo.ca/drupal-org/token.git"
projects[token][download][tag] = "7.x-1.9"
projects[token][subdir] = "contrib"

; Transliteration
projects[transliteration][type] = "module"
projects[transliteration][download][type] = "git"
projects[transliteration][download][url] = "https://git.uwaterloo.ca/drupal-org/transliteration.git"
projects[transliteration][download][tag] = "7.x-3.2"
projects[transliteration][subdir] = "contrib"

; TVI - Taxonomy Views Integrator
projects[tvi][type] = "module"
projects[tvi][download][type] = "git"
projects[tvi][download][url] = "https://git.uwaterloo.ca/drupal-org/tvi.git"
projects[tvi][download][tag] = "7.x-1.0"
projects[tvi][subdir] = "contrib"

; User protect
; Base: 7.x-1.3
; Patch 426e271: Prevent users from being able to cancel their own accounts when protected. https://www.drupal.org/project/userprotect/issues/1969086#comment-12076133
projects[userprotect][type] = "module"
projects[userprotect][download][type] = "git"
projects[userprotect][download][url] = "https://git.uwaterloo.ca/drupal-org/userprotect.git"
projects[userprotect][download][tag] = "7.x-1.3-uw_wcms1"
projects[userprotect][subdir] = "contrib"

; UUID
projects[uuid][type] = "module"
projects[uuid][download][type] = "git"
projects[uuid][download][url] = "https://git.uwaterloo.ca/drupal-org/uuid.git"
projects[uuid][download][tag] = "7.x-1.3"
projects[uuid][subdir] = "contrib"

; UUID Features
; Base: 7.x-1.0-rc2
; Patch a688a3a: Issue #2158057: UUID support for default value in taxonomy term reference field settings. https://www.drupal.org/project/uuid_features/issues/2158057#comment-13369588
projects[uuid_features][type] = "module"
projects[uuid_features][download][type] = "git"
projects[uuid_features][download][url] = "https://git.uwaterloo.ca/drupal-org/uuid_features.git"
projects[uuid_features][download][tag] = "7.x-1.0-rc2-uw_wcms1"
projects[uuid_features][subdir] = "contrib"

; Variable
projects[variable][type] = "module"
projects[variable][download][type] = "git"
projects[variable][download][url] = "https://git.uwaterloo.ca/drupal-org/variable.git"
projects[variable][download][tag] = "7.x-2.5"
projects[variable][subdir] = "contrib"

; Varnish
projects[varnish][type] = "module"
projects[varnish][download][type] = "git"
projects[varnish][download][url] = "https://git.uwaterloo.ca/drupal-org/varnish.git"
projects[varnish][download][tag] = "7.x-1.9"
projects[varnish][subdir] = "contrib"

; Views
projects[views][type] = "module"
projects[views][download][type] = "git"
projects[views][download][url] = "https://git.uwaterloo.ca/drupal-org/views.git"
projects[views][download][tag] = "7.x-3.27"
projects[views][subdir] = "contrib"

; Views Bulk Operations
projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][download][type] = "git"
projects[views_bulk_operations][download][url] = "https://git.uwaterloo.ca/drupal-org/views_bulk_operations.git"
projects[views_bulk_operations][download][tag] = "7.x-3.7"
projects[views_bulk_operations][subdir] = "contrib"

; Views Data Export
; Base: 7.x-3.2
; Patch: 5998258 Exception caused by MySQL 5.6 gtid replication. https://www.drupal.org/project/views_data_export/issues/2209853#comment-13109347
projects[views_data_export][type] = "module"
projects[views_data_export][download][type] = "git"
projects[views_data_export][download][url] = "https://git.uwaterloo.ca/drupal-org/views_data_export.git"
projects[views_data_export][download][tag] = "7.x-3.2-uw_wcms1"
projects[views_data_export][subdir] = "contrib"

; Views Field View
projects[views_field_view][type] = "module"
projects[views_field_view][download][type] = "git"
projects[views_field_view][download][url] = "https://git.uwaterloo.ca/drupal-org/views_field_view.git"
projects[views_field_view][download][tag] = "7.x-1.2"
projects[views_field_view][subdir] = "contrib"

; Views iCal
projects[views_ical][type] = "module"
projects[views_ical][download][type] = "git"
projects[views_ical][download][url] = "https://git.uwaterloo.ca/drupal-org/views_ical.git"
projects[views_ical][download][tag] = "7.x-1.0-beta2"
projects[views_ical][subdir] = "contrib"

; Views Load More
projects[views_load_more][type] = "module"
projects[views_load_more][download][type] = "git"
projects[views_load_more][download][url] = "https://git.uwaterloo.ca/drupal-org/views_load_more.git"
projects[views_load_more][download][tag] = "7.x-1.5"
projects[views_load_more][subdir] = "contrib"

; Views merge rows
projects[views_merge_rows][type] = "module"
projects[views_merge_rows][download][type] = "git"
projects[views_merge_rows][download][url] = "https://git.uwaterloo.ca/drupal-org/views_merge_rows.git"
projects[views_merge_rows][download][tag] = "7.x-1.0-rc1"
projects[views_merge_rows][subdir] = "contrib"

; Views PHP
projects[views_php][type] = "module"
projects[views_php][download][type] = "git"
projects[views_php][download][url] = "https://git.uwaterloo.ca/drupal-org/views_php.git"
projects[views_php][download][tag] = "7.x-1.1"
projects[views_php][subdir] = "contrib"

; Views Raw SQL
projects[views_raw_sql][type] = "module"
projects[views_raw_sql][download][type] = "git"
projects[views_raw_sql][download][url] = "https://git.uwaterloo.ca/drupal-org/views_raw_sql.git"
projects[views_raw_sql][download][tag] = "7.x-1.1"
projects[views_raw_sql][subdir] = "contrib"

; Views Sort Null Field
projects[views_sort_null_field][type] = "module"
projects[views_sort_null_field][download][type] = "git"
projects[views_sort_null_field][download][url] = "https://git.uwaterloo.ca/drupal-org/views_sort_null_field.git"
projects[views_sort_null_field][download][tag] = "7.x-1.0"
projects[views_sort_null_field][subdir] = "contrib"

; Views Timeline JS integration
; Base: 7.x-1.1
; Patch f9d34d6: Issue #2602880: Support alt text for images. https://www.drupal.org/node/2602880#comment-10497834
projects[views_timelinejs][type] = "module"
projects[views_timelinejs][download][type] = "git"
projects[views_timelinejs][download][url] = "https://git.uwaterloo.ca/drupal-org/views_timelinejs.git"
projects[views_timelinejs][download][tag] = "7.x-1.1-uw_wcms1"
projects[views_timelinejs][subdir] = "contrib"

; Webform
projects[webform][type] = "module"
projects[webform][download][type] = "git"
projects[webform][download][url] = "https://git.uwaterloo.ca/drupal-org/webform.git"
projects[webform][download][tag] = "7.x-4.24"
projects[webform][subdir] = "contrib"

; Webform Access Granular
projects[webform_access_granular][type] = "module"
projects[webform_access_granular][download][type] = "git"
projects[webform_access_granular][download][url] = "https://git.uwaterloo.ca/drupal-org/webform_access_granular.git"
projects[webform_access_granular][download][tag] = "7.x-1.20"
projects[webform_access_granular][subdir] = "contrib"

; Webform Rules
projects[webform_rules][type] = "module"
projects[webform_rules][download][type] = "git"
projects[webform_rules][download][url] = "https://git.uwaterloo.ca/drupal-org/webform_rules.git"
projects[webform_rules][download][tag] = "7.x-1.6"
projects[webform_rules][subdir] = "contrib"

; Webform Validation
projects[webform_validation][type] = "module"
projects[webform_validation][download][type] = "git"
projects[webform_validation][download][url] = "https://git.uwaterloo.ca/drupal-org/webform_validation.git"
projects[webform_validation][download][tag] = "7.x-1.18"
projects[webform_validation][subdir] = "contrib"

; Workbench
; Base: 7.x-1.2
; Patch bd77ef7: RT#393703: Remove contextual link error with Patch 13 of https://www.drupal.org/node/1727284.
projects[workbench][type] = "module"
projects[workbench][download][type] = "git"
projects[workbench][download][url] = "https://git.uwaterloo.ca/drupal-org/workbench.git"
projects[workbench][download][tag] = "7.x-1.2-uw_wcms1"
projects[workbench][subdir] = "contrib"

; Workbench Access
projects[workbench_access][type] = "module"
projects[workbench_access][download][type] = "git"
projects[workbench_access][download][url] = "https://git.uwaterloo.ca/drupal-org/workbench_access.git"
projects[workbench_access][download][tag] = "7.x-1.6"
projects[workbench_access][subdir] = "contrib"

; Workbench Moderation
; Base: 7.x-1.4
; Patch 643be34: Upgrade from 1.3 to 1.4 with drush fails. https://www.drupal.org/node/2428371
; Patch e6e0ae3: Revert "Issue #1492118 by joelpittet, jmuzz, nasia123, bbinkovitz, delphian: Set up access grants for the unpublished content access permission." This broke publication ordering by adding a bad node access grant to each sql query.
; Patch 7c53fb3: invokes a call to the Pathauto pathauto_entity_state_load() function to set the value correctly. https://www.drupal.org/node/2616854
; Patch e141ceb: Start compatibility with restws module. https://www.drupal.org/node/1838640#comment-9819815
; Patch fca021c: Complete compatibility with restws module. Issue #1838640. Cherry-pick from 6356bd2.
; Patch fa77d43: PHP 7 support. Issue #2871962. Cherry-pick from 541aac7.
projects[workbench_moderation][type] = "module"
projects[workbench_moderation][download][type] = "git"
projects[workbench_moderation][download][url] = "https://git.uwaterloo.ca/drupal-org/workbench_moderation.git"
projects[workbench_moderation][download][tag] = "7.x-1.4-uw_wcms5"
projects[workbench_moderation][subdir] = "contrib"

; WYSIWYG
projects[wysiwyg][type] = "module"
projects[wysiwyg][download][type] = "git"
projects[wysiwyg][download][url] = "https://git.uwaterloo.ca/drupal-org/wysiwyg.git"
projects[wysiwyg][download][branch] = "7.x-2.x"
projects[wysiwyg][download][revision] = "8dd13f4"
projects[wysiwyg][subdir] = "contrib"

; WYSIWYG A11ychecker
projects[wysiwyg_a11ychecker][type] = "module"
projects[wysiwyg_a11ychecker][download][type] = "git"
projects[wysiwyg_a11ychecker][download][url] = "https://git.uwaterloo.ca/drupal-org/wysiwyg_a11ychecker.git"
projects[wysiwyg_a11ychecker][download][tag] = "7.x-1.0"
projects[wysiwyg_a11ychecker][subdir] = "contrib"

; WYSIWYG Filter
projects[wysiwyg_filter][type] = "module"
projects[wysiwyg_filter][download][type] = "git"
projects[wysiwyg_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/wysiwyg_filter.git"
projects[wysiwyg_filter][download][tag] = "7.x-1.6-rc9"
projects[wysiwyg_filter][subdir] = "contrib"

; XML sitemap
; Base: 86000db (7.x-2.6+19-dev)
; Patch de88012: Change frequency option for nodes is added. https://www.drupal.org/project/xmlsitemap/issues/1811692#comment-13072305
projects[xmlsitemap][type] = "module"
projects[xmlsitemap][download][type] = "git"
projects[xmlsitemap][download][url] = "https://git.uwaterloo.ca/drupal-org/xmlsitemap.git"
projects[xmlsitemap][download][tag] = "7.x-2.6+19-dev-uw_wcms1"
projects[xmlsitemap][subdir] = "contrib"

; tag1_d7es
projects[tag1_d7es][type] = "module"
projects[tag1_d7es][download][type] = "git"
projects[tag1_d7es][download][url] = "https://git.uwaterloo.ca/drupal-org/tag1_d7es.git"
projects[tag1_d7es][download][branch] = "7.x-1.2"
projects[tag1_d7es][subdir] = "contrib"

;------------------------------------------------------------------------------------------------------------------------
; Custom Modules
; These modules are developed in-house
;------------------------------------------------------------------------------------------------------------------------

; CKEditor Social Media
projects[ckeditor_socialmedia][type] = "module"
projects[ckeditor_socialmedia][download][type] = "git"
projects[ckeditor_socialmedia][download][url] = "https://git.uwaterloo.ca/wcms/ckeditor_socialmedia.git"
projects[ckeditor_socialmedia][download][branch] = "7.x-1.x"
projects[ckeditor_socialmedia][subdir] = "custom"

; Date Friendly Abbr.
projects[date_friendly_abbr][type] = "module"
projects[date_friendly_abbr][download][type] = "git"
projects[date_friendly_abbr][download][url] = "https://git.uwaterloo.ca/wcms/date_friendly_abbr.git"
projects[date_friendly_abbr][download][branch] = "7.x-1.x"
projects[date_friendly_abbr][subdir] = "custom"

; Link autocomplete
projects[link_autocomplete][type] = "module"
projects[link_autocomplete][download][type] = "git"
projects[link_autocomplete][download][url] = "https://git.uwaterloo.ca/wcms/link_autocomplete.git"
projects[link_autocomplete][download][branch] = "7.x-1.x"
projects[link_autocomplete][subdir] = "custom"

; Media Link
projects[media_link][type] = "module"
projects[media_link][download][type] = "git"
projects[media_link][download][url] = "https://git.uwaterloo.ca/wcms/media_link.git"
projects[media_link][download][branch] = "7.x-1.x"
projects[media_link][subdir] = "custom"

; Menu Operations Permission module
projects[menu_operations_permission][type] = "module"
projects[menu_operations_permission][download][type] = "git"
projects[menu_operations_permission][download][url] = "https://git.uwaterloo.ca/wcms/menu_operations_permission.git"
projects[menu_operations_permission][download][branch] = "7.x-1.x"
projects[menu_operations_permission][subdir] = "custom"

; uWaterloo Accessibility
projects[uw_a11y][type] = "module"
projects[uw_a11y][download][type] = "git"
projects[uw_a11y][download][url] = "https://git.uwaterloo.ca/wcms/uw_a11y.git"
projects[uw_a11y][download][branch] = "7.x-1.x"
projects[uw_a11y][subdir] = "custom"

; Admin usability - form menu
projects[uw_adminux_form_menu][type] = "module"
projects[uw_adminux_form_menu][download][type] = "git"
projects[uw_adminux_form_menu][download][url] = "https://git.uwaterloo.ca/wcms/uw_adminux_form_menu.git"
projects[uw_adminux_form_menu][download][branch] = "7.x-1.x"
projects[uw_adminux_form_menu][subdir] = "custom"

; Assign WCMS Groups from Active Directory
projects[uw_auth_wcms_admins][type] = "module"
projects[uw_auth_wcms_admins][download][type] = "git"
projects[uw_auth_wcms_admins][download][url] = "https://git.uwaterloo.ca/wcms/uw_auth_wcms_admins.git"
projects[uw_auth_wcms_admins][download][branch] = "7.x-1.x"
projects[uw_auth_wcms_admins][subdir] = "custom"

; UW CKEditor Link Paths
projects[uw_ckeditor_link_paths][type] = "module"
projects[uw_ckeditor_link_paths][download][type] = "git"
projects[uw_ckeditor_link_paths][download][url] = "https://git.uwaterloo.ca/wcms/uw_ckeditor_link_paths.git"
projects[uw_ckeditor_link_paths][download][branch] = "7.x-1.x"
projects[uw_ckeditor_link_paths][subdir] = "custom"

; Clear Cache Block
projects[uw_clear_cache_block][type] = "module"
projects[uw_clear_cache_block][download][type] = "git"
projects[uw_clear_cache_block][download][url] = "https://git.uwaterloo.ca/wcms/uw_clear_cache_block.git"
projects[uw_clear_cache_block][download][branch] = "7.x-1.x"
projects[uw_clear_cache_block][subdir] = "custom"

; Conference Admin
projects[uw_conference_admin][type] = "module"
projects[uw_conference_admin][download][type] = "git"
projects[uw_conference_admin][download][url] = "https://git.uwaterloo.ca/wcms/uw_conference_admin.git"
projects[uw_conference_admin][download][branch] = "7.x-1.x"
projects[uw_conference_admin][subdir] = "custom"

; UW Content Type Use
projects[uw_content_type_use][type] = "module"
projects[uw_content_type_use][download][type] = "git"
projects[uw_content_type_use][download][url] = "https://git.uwaterloo.ca/wcms/uw_content_type_use.git"
projects[uw_content_type_use][download][branch] = "7.x-1.x"
projects[uw_content_type_use][subdir] = "custom"

; Default Audience Categories
projects[uw_data_audience_categories][type] = "module"
projects[uw_data_audience_categories][download][type] = "git"
projects[uw_data_audience_categories][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_audience_categories.git"
projects[uw_data_audience_categories][download][branch] = "7.x-1.x"
projects[uw_data_audience_categories][subdir] = "custom"

; Default Feature Story Categories
projects[uw_data_feature_story_categories][type] = "module"
projects[uw_data_feature_story_categories][download][type] = "git"
projects[uw_data_feature_story_categories][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_feature_story_categories.git"
projects[uw_data_feature_story_categories][download][branch] = "7.x-1.x"
projects[uw_data_feature_story_categories][subdir] = "custom"

; Default Profile Categories
projects[uw_data_profile_categories][type] = "module"
projects[uw_data_profile_categories][download][type] = "git"
projects[uw_data_profile_categories][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_profile_categories.git"
projects[uw_data_profile_categories][download][branch] = "7.x-1.x"
projects[uw_data_profile_categories][subdir] = "custom"

; Default Web Pages and site IA, run during site install time and setup
projects[uw_data_web_pages][type] = "module"
projects[uw_data_web_pages][download][type] = "git"
projects[uw_data_web_pages][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_web_pages.git"
projects[uw_data_web_pages][download][branch] = "7.x-1.x"
projects[uw_data_web_pages][subdir] = "custom"

; uWaterloo Decoupled Module [Publication]
projects[uw_decoupled_module][type] = "module"
projects[uw_decoupled_module][download][type] = "git"
projects[uw_decoupled_module][download][url] = "https://git.uwaterloo.ca/wcms-decoupled/uw_decoupled_module.git"
projects[uw_decoupled_module][download][branch] = "7.x-2.x"
projects[uw_decoupled_module][subdir] = "custom"

; Display Writer Tips
projects[uw_display_writer_tips][type] = "module"
projects[uw_display_writer_tips][download][type] = "git"
projects[uw_display_writer_tips][download][url] = "https://git.uwaterloo.ca/wcms/uw_display_writer_tips.git"
projects[uw_display_writer_tips][download][branch] = "7.x-1.x"
projects[uw_display_writer_tips][subdir] = "custom"

; CKEditor Embedded Maps
projects[uw_embedded_maps][type] = "module"
projects[uw_embedded_maps][download][type] = "git"
projects[uw_embedded_maps][download][url] = "https://git.uwaterloo.ca/wcms/uw_embedded_maps.git"
projects[uw_embedded_maps][download][branch] = "7.x-1.x"
projects[uw_embedded_maps][subdir] = "custom"

; uWaterloo Tableau Field
projects[uw_field_tableau][type] = "module"
projects[uw_field_tableau][download][type] = "git"
projects[uw_field_tableau][download][url] = "https://git.uwaterloo.ca/wcms/uw_field_tableau.git"
projects[uw_field_tableau][download][branch] = "7.x-1.x"
projects[uw_field_tableau][subdir] = "custom"

; uWaterloo Filters
projects[uw_filters][type] = "module"
projects[uw_filters][download][type] = "git"
projects[uw_filters][download][url] = "https://git.uwaterloo.ca/wcms/uw_filters.git"
projects[uw_filters][download][branch] = "7.x-1.x"
projects[uw_filters][subdir] = "custom"

; Web Form Additional Functionality
projects[uw_forms][type] = "module"
projects[uw_forms][download][type] = "git"
projects[uw_forms][download][url] = "https://git.uwaterloo.ca/wcms/uw_forms.git"
projects[uw_forms][download][branch] = "7.x-1.x"
projects[uw_forms][subdir] = "custom"

; uWaterloo Header Search
projects[uw_header_search][type] = "module"
projects[uw_header_search][download][type] = "git"
projects[uw_header_search][download][url] = "https://git.uwaterloo.ca/wcms/uw_header_search.git"
projects[uw_header_search][download][branch] = "7.x-1.x"
projects[uw_header_search][subdir] = "custom"

; uWaterloo LDAP
projects[uw_ldap][type] = "module"
projects[uw_ldap][download][type] = "git"
projects[uw_ldap][download][url] = "https://git.uwaterloo.ca/wcms/uw_ldap.git"
projects[uw_ldap][download][branch] = "7.x-1.x"
projects[uw_ldap][subdir] = "custom"

; uWaterloo Node Reference Insert [Publication]
projects[uw_node_reference_insert][type] = "module"
projects[uw_node_reference_insert][download][type] = "git"
projects[uw_node_reference_insert][download][url] = "https://git.uwaterloo.ca/wcms/uw_node_reference_insert.git"
projects[uw_node_reference_insert][download][branch] = "7.x-1.x"
projects[uw_node_reference_insert][subdir] = "custom"

; uWaterloo Org Chart
projects[uw_org_chart][type] = "module"
projects[uw_org_chart][download][type] = "git"
projects[uw_org_chart][download][url] = "https://git.uwaterloo.ca/wcms/uw_org_chart.git"
projects[uw_org_chart][download][branch] = "7.x-1.x"
projects[uw_org_chart][subdir] = "custom"

; uWaterloo Page settings
projects[uw_page_settings_node][type] = "module"
projects[uw_page_settings_node][download][type] = "git"
projects[uw_page_settings_node][download][url] = "https://git.uwaterloo.ca/wcms/uw_page_settings_node.git"
projects[uw_page_settings_node][download][branch] = "7.x-1.x"
projects[uw_page_settings_node][subdir] = "custom"

; uWaterloo RESTful Publication [Publication]
projects[uw_restful_publication][type] = "module"
projects[uw_restful_publication][download][type] = "git"
projects[uw_restful_publication][download][url] = "https://git.uwaterloo.ca/wcms-decoupled/uw_restful_publication.git"
projects[uw_restful_publication][download][branch] = "7.x-1.x"
projects[uw_restful_publication][subdir] = "custom"

; uWaterloo Slack
projects[uw_slack][type] = "module"
projects[uw_slack][download][type] = "git"
projects[uw_slack][download][url] = "https://git.uwaterloo.ca/wcms/uw_slack.git"
projects[uw_slack][download][branch] = "7.x-1.x"
projects[uw_slack][subdir] = "custom"

; uWaterloo Social media sharing
projects[uw_social_media_sharing][type] = "module"
projects[uw_social_media_sharing][download][type] = "git"
projects[uw_social_media_sharing][download][url] = "https://git.uwaterloo.ca/wcms/uw_social_media_sharing.git"
projects[uw_social_media_sharing][download][branch] = "7.x-1.x"
projects[uw_social_media_sharing][subdir] = "custom"

; uWaterloo Taxonomy Rights Access
projects[uw_taxonomy_rights_access][type] = "module"
projects[uw_taxonomy_rights_access][download][type] = "git"
projects[uw_taxonomy_rights_access][download][url] = "https://git.uwaterloo.ca/wcms/uw_taxonomy_rights_access.git"
projects[uw_taxonomy_rights_access][download][branch] = "7.x-1.x"
projects[uw_taxonomy_rights_access][subdir] = "custom"

; uWaterloo Video Embed
projects[uw_video_embed][type] = "module"
projects[uw_video_embed][download][type] = "git"
projects[uw_video_embed][download][url] = "https://git.uwaterloo.ca/wcms/uw_video_embed.git"
projects[uw_video_embed][download][branch] = "7.x-1.x"
projects[uw_video_embed][subdir] = "custom"

; UW Who's Online
projects[uw_whos_online][type] = "module"
projects[uw_whos_online][download][type] = "git"
projects[uw_whos_online][download][url] = "https://git.uwaterloo.ca/wcms/uw_whos_online.git"
projects[uw_whos_online][download][branch] = "7.x-1.x"
projects[uw_whos_online][subdir] = "custom"

; uWaterloo OFIS
projects[uw_ws_ofis][type] = "module"
projects[uw_ws_ofis][download][type] = "git"
projects[uw_ws_ofis][download][url] = "https://git.uwaterloo.ca/wcms/uw_ws_ofis.git"
projects[uw_ws_ofis][download][branch] = "7.x-1.x"
projects[uw_ws_ofis][subdir] = "custom"


;------------------------------------------------------------------------------------------------------------------------
; Features
; Modules that store config information in code instead of the database
;------------------------------------------------------------------------------------------------------------------------

; Anonymous Blog Comment Overrides
projects[uw_anonymous_blog_comment_overrides][type] = "module"
projects[uw_anonymous_blog_comment_overrides][download][type] = "git"
projects[uw_anonymous_blog_comment_overrides][download][url] = "https://git.uwaterloo.ca/wcms/uw_anonymous_blog_comment_overrides.git"
projects[uw_anonymous_blog_comment_overrides][download][branch] = "7.x-1.x"
projects[uw_anonymous_blog_comment_overrides][subdir] = "features"

; CAS Authentication Common
projects[uw_auth_cas_common][type] = "module"
projects[uw_auth_cas_common][download][type] = "git"
projects[uw_auth_cas_common][download][url] = "https://git.uwaterloo.ca/wcms/uw_auth_cas_common.git"
projects[uw_auth_cas_common][download][branch] = "7.x-1.x"
projects[uw_auth_cas_common][subdir] = "features"

; Authentication Required Site
projects[uw_auth_site][type] = "module"
projects[uw_auth_site][download][type] = "git"
projects[uw_auth_site][download][url] = "https://git.uwaterloo.ca/wcms/uw_auth_site.git"
projects[uw_auth_site][download][branch] = "7.x-1.x"
projects[uw_auth_site][subdir] = "features"

; uWaterloo CAPTCHA
projects[uw_captcha][type] = "module"
projects[uw_captcha][download][type] = "git"
projects[uw_captcha][download][url] = "https://git.uwaterloo.ca/wcms/uw_captcha.git"
projects[uw_captcha][download][branch] = "7.x-2.x"
projects[uw_captcha][subdir] = "features"

; Chinese Views Config
projects[uw_cfg_chinese_views][type] = "module"
projects[uw_cfg_chinese_views][download][type] = "git"
projects[uw_cfg_chinese_views][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_chinese_views.git"
projects[uw_cfg_chinese_views][download][branch] = "7.x-2.x"
projects[uw_cfg_chinese_views][subdir] = "features"

; Clone Web Pages Config
projects[uw_cfg_clone_web_pages][type] = "module"
projects[uw_cfg_clone_web_pages][download][type] = "git"
projects[uw_cfg_clone_web_pages][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_clone_web_pages.git"
projects[uw_cfg_clone_web_pages][download][branch] = "7.x-1.x"
projects[uw_cfg_clone_web_pages][subdir] = "features"

; Conference Config
projects[uw_cfg_conference][type] = "module"
projects[uw_cfg_conference][download][type] = "git"
projects[uw_cfg_conference][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_conference.git"
projects[uw_cfg_conference][download][branch] = "7.x-1.x"
projects[uw_cfg_conference][subdir] = "features"

; Conference Override Config
projects[uw_cfg_conference_overrides][type] = "module"
projects[uw_cfg_conference_overrides][download][type] = "git"
projects[uw_cfg_conference_overrides][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_conference_overrides.git"
projects[uw_cfg_conference_overrides][download][branch] = "7.x-1.x"
projects[uw_cfg_conference_overrides][subdir] = "features"

; Devel Config
projects[uw_cfg_devel][type] = "module"
projects[uw_cfg_devel][download][type] = "git"
projects[uw_cfg_devel][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_devel.git"
projects[uw_cfg_devel][download][branch] = "7.x-1.x"
projects[uw_cfg_devel][subdir] = "features"

; uWaterloo Expire Config
projects[uw_cfg_expire][type] = "module"
projects[uw_cfg_expire][download][type] = "git"
projects[uw_cfg_expire][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_expire.git"
projects[uw_cfg_expire][download][branch] = "7.x-1.x"
projects[uw_cfg_expire][subdir] = "features"

; Google Analytics Config
projects[uw_cfg_google_analytics][type] = "module"
projects[uw_cfg_google_analytics][download][type] = "git"
projects[uw_cfg_google_analytics][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_google_analytics.git"
projects[uw_cfg_google_analytics][download][branch] = "7.x-1.x"
projects[uw_cfg_google_analytics][subdir] = "features"

; Metatag Config
projects[uw_cfg_metatag][type] = "module"
projects[uw_cfg_metatag][download][type] = "git"
projects[uw_cfg_metatag][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_metatag.git"
projects[uw_cfg_metatag][download][branch] = "7.x-1.x"
projects[uw_cfg_metatag][subdir] = "features"

; uWaterloo Publication [Publication]
projects[uw_cfg_publication][type] = "module"
projects[uw_cfg_publication][download][type] = "git"
projects[uw_cfg_publication][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_publication.git"
projects[uw_cfg_publication][download][branch] = "7.x-2.x"
projects[uw_cfg_publication][subdir] = "features"

; Redirect Config
projects[uw_cfg_redirect][type] = "module"
projects[uw_cfg_redirect][download][type] = "git"
projects[uw_cfg_redirect][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_redirect.git"
projects[uw_cfg_redirect][download][branch] = "7.x-1.x"
projects[uw_cfg_redirect][subdir] = "features"

; Responsive config
projects[uw_cfg_responsive][type] = "module"
projects[uw_cfg_responsive][download][type] = "git"
projects[uw_cfg_responsive][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_responsive.git"
projects[uw_cfg_responsive][download][branch] = "7.x-1.x"
projects[uw_cfg_responsive][subdir] = "features"

; uWaterloo Single Page
projects[uw_cfg_single_page][type] = "module"
projects[uw_cfg_single_page][download][type] = "git"
projects[uw_cfg_single_page][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_single_page.git"
projects[uw_cfg_single_page][download][branch] = "7.x-1.x"
projects[uw_cfg_single_page][subdir] = "features"

; Academic Calendar
projects[uw_ct_academic_calendar][type] = "module"
projects[uw_ct_academic_calendar][download][type] = "git"
projects[uw_ct_academic_calendar][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_academic_calendar.git"
projects[uw_ct_academic_calendar][download][branch] = "7.x-1.x"
projects[uw_ct_academic_calendar][subdir] = "features"

; Award
projects[uw_ct_award][type] = "module"
projects[uw_ct_award][download][type] = "git"
projects[uw_ct_award][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_award.git"
projects[uw_ct_award][download][branch] = "7.x-2.x"
projects[uw_ct_award][subdir] = "features"

; Bibliography
projects[uw_ct_bibliography][type] = "module"
projects[uw_ct_bibliography][download][type] = "git"
projects[uw_ct_bibliography][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_bibliography.git"
projects[uw_ct_bibliography][download][branch] = "7.x-2.x"
projects[uw_ct_bibliography][subdir] = "features"

; Blog
projects[uw_ct_blog][type] = "module"
projects[uw_ct_blog][download][type] = "git"
projects[uw_ct_blog][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_blog.git"
projects[uw_ct_blog][download][branch] = "7.x-2.x"
projects[uw_ct_blog][subdir] = "features"

; Catalog
projects[uw_ct_catalog][type] = "module"
projects[uw_ct_catalog][download][type] = "git"
projects[uw_ct_catalog][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_catalog.git"
projects[uw_ct_catalog][download][branch] = "7.x-1.x"
projects[uw_ct_catalog][subdir] = "features"

; Contact
projects[uw_ct_contact][type] = "module"
projects[uw_ct_contact][download][type] = "git"
projects[uw_ct_contact][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_contact.git"
projects[uw_ct_contact][download][branch] = "7.x-2.x"
projects[uw_ct_contact][subdir] = "features"

; Custom Listing Page
projects[uw_ct_custom_listing][type] = "module"
projects[uw_ct_custom_listing][download][type] = "git"
projects[uw_ct_custom_listing][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_custom_listing.git"
projects[uw_ct_custom_listing][download][branch] = "7.x-1.x"
projects[uw_ct_custom_listing][subdir] = "features"

; Embedded call to action
projects[uw_ct_embedded_call_to_action][type] = "module"
projects[uw_ct_embedded_call_to_action][download][type] = "git"
projects[uw_ct_embedded_call_to_action][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_embedded_call_to_action.git"
projects[uw_ct_embedded_call_to_action][download][branch] = "7.x-1.x"
projects[uw_ct_embedded_call_to_action][subdir] = "features"

; Embedded Facts and Figures
projects[uw_ct_embedded_facts_and_figures][type] = "module"
projects[uw_ct_embedded_facts_and_figures][download][type] = "git"
projects[uw_ct_embedded_facts_and_figures][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_embedded_facts_and_figures.git"
projects[uw_ct_embedded_facts_and_figures][download][branch] = "7.x-1.x"
projects[uw_ct_embedded_facts_and_figures][subdir] = "features"

; Embedded Timeline
projects[uw_ct_embedded_timeline][type] = "module"
projects[uw_ct_embedded_timeline][download][type] = "git"
projects[uw_ct_embedded_timeline][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_embedded_timeline.git"
projects[uw_ct_embedded_timeline][download][branch] = "7.x-1.x"
projects[uw_ct_embedded_timeline][subdir] = "features"

; Event
projects[uw_ct_event][type] = "module"
projects[uw_ct_event][download][type] = "git"
projects[uw_ct_event][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_event.git"
projects[uw_ct_event][download][branch] = "7.x-2.x"
projects[uw_ct_event][subdir] = "features"

; Home Page Banner
projects[uw_ct_home_page_banner][type] = "module"
projects[uw_ct_home_page_banner][download][type] = "git"
projects[uw_ct_home_page_banner][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_home_page_banner.git"
projects[uw_ct_home_page_banner][download][branch] = "7.x-2.x"
projects[uw_ct_home_page_banner][subdir] = "features"

; Image Gallery
projects[uw_ct_image_gallery][type] = "module"
projects[uw_ct_image_gallery][download][type] = "git"
projects[uw_ct_image_gallery][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_image_gallery.git"
projects[uw_ct_image_gallery][download][branch] = "7.x-2.x"
projects[uw_ct_image_gallery][subdir] = "features"

; News Item
projects[uw_ct_news_item][type] = "module"
projects[uw_ct_news_item][download][type] = "git"
projects[uw_ct_news_item][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_news_item.git"
projects[uw_ct_news_item][download][branch] = "7.x-2.x"
projects[uw_ct_news_item][subdir] = "features"

; Opportunities
projects[uw_ct_opportunities][type] = "module"
projects[uw_ct_opportunities][download][type] = "git"
projects[uw_ct_opportunities][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_opportunities.git"
projects[uw_ct_opportunities][download][branch] = "7.x-2.x"
projects[uw_ct_opportunities][subdir] = "features"

; Person Profile
projects[uw_ct_person_profile][type] = "module"
projects[uw_ct_person_profile][download][type] = "git"
projects[uw_ct_person_profile][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_person_profile.git"
projects[uw_ct_person_profile][download][branch] = "7.x-2.x"
projects[uw_ct_person_profile][subdir] = "features"

; PowerBI
projects[uw_embedded_powerbi][type] = "module"
projects[uw_embedded_powerbi][download][type] = "git"
projects[uw_embedded_powerbi][download][url] = "https://git.uwaterloo.ca/wcms/uw_embedded_powerbi.git"
projects[uw_embedded_powerbi][download][branch] = "7.x-1.x"
projects[uw_embedded_powerbi][subdir] = "features"

; Project
projects[uw_ct_project][type] = "module"
projects[uw_ct_project][download][type] = "git"
projects[uw_ct_project][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_project.git"
projects[uw_ct_project][download][branch] = "7.x-1.x"
projects[uw_ct_project][subdir] = "features"

; Promotional Item
projects[uw_ct_promo_item][type] = "module"
projects[uw_ct_promo_item][download][type] = "git"
projects[uw_ct_promo_item][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_promo_item.git"
projects[uw_ct_promo_item][download][branch] = "7.x-2.x"
projects[uw_ct_promo_item][subdir] = "features"

; Service
projects[uw_ct_service][type] = "module"
projects[uw_ct_service][download][type] = "git"
projects[uw_ct_service][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_service.git"
projects[uw_ct_service][download][branch] = "7.x-2.x"
projects[uw_ct_service][subdir] = "features"

; Single Page Home
projects[uw_ct_single_page_home][type] = "module"
projects[uw_ct_single_page_home][download][type] = "git"
projects[uw_ct_single_page_home][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_single_page_home.git"
projects[uw_ct_single_page_home][download][branch] = "7.x-1.x"
projects[uw_ct_single_page_home][subdir] = "features"

; Special Alert
projects[uw_ct_special_alert][type] = "module"
projects[uw_ct_special_alert][download][type] = "git"
projects[uw_ct_special_alert][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_special_alert.git"
projects[uw_ct_special_alert][download][branch] = "7.x-2.x"
projects[uw_ct_special_alert][subdir] = "features"

; Web Form
projects[uw_ct_web_form][type] = "module"
projects[uw_ct_web_form][download][type] = "git"
projects[uw_ct_web_form][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_web_form.git"
projects[uw_ct_web_form][download][branch] = "7.x-1.x"
projects[uw_ct_web_form][subdir] = "features"

; Web Page
projects[uw_ct_web_page][type] = "module"
projects[uw_ct_web_page][download][type] = "git"
projects[uw_ct_web_page][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_web_page.git"
projects[uw_ct_web_page][download][branch] = "7.x-2.x"
projects[uw_ct_web_page][subdir] = "features"

; FDSU Dashboard
projects[uw_dashboard_fdsu][type] = "module"
projects[uw_dashboard_fdsu][download][type] = "git"
projects[uw_dashboard_fdsu][download][url] = "https://git.uwaterloo.ca/wcms/uw_dashboard_fdsu.git"
projects[uw_dashboard_fdsu][download][branch] = "7.x-1.x"
projects[uw_dashboard_fdsu][subdir] = "features"

; Web Resources News Feed
projects[uw_feed_webresources_news][type] = "module"
projects[uw_feed_webresources_news][download][type] = "git"
projects[uw_feed_webresources_news][download][url] = "https://git.uwaterloo.ca/wcms/uw_feed_webresources_news.git"
projects[uw_feed_webresources_news][download][branch] = "7.x-1.x"
projects[uw_feed_webresources_news][subdir] = "features"

; uWaterloo Gmap Config
projects[uw_gmap_config][type] = "module"
projects[uw_gmap_config][download][type] = "git"
projects[uw_gmap_config][download][url] = "https://git.uwaterloo.ca/wcms/uw_gmap_config.git"
projects[uw_gmap_config][download][branch] = "7.x-1.x"
projects[uw_gmap_config][subdir] = "features"

; uWaterloo Less Transliteration
projects[uw_less_transliteration][type] = "module"
projects[uw_less_transliteration][download][type] = "git"
projects[uw_less_transliteration][download][url] = "https://git.uwaterloo.ca/wcms/uw_less_transliteration.git"
projects[uw_less_transliteration][download][branch] = "7.x-1.x"
projects[uw_less_transliteration][subdir] = "features"

; Audience Menu
projects[uw_nav_audience_menu][type] = "module"
projects[uw_nav_audience_menu][download][type] = "git"
projects[uw_nav_audience_menu][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_audience_menu.git"
projects[uw_nav_audience_menu][download][branch] = "7.x-1.x"
projects[uw_nav_audience_menu][subdir] = "features"

; Global Footer
projects[uw_nav_global_footer][type] = "module"
projects[uw_nav_global_footer][download][type] = "git"
projects[uw_nav_global_footer][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_global_footer.git"
projects[uw_nav_global_footer][download][branch] = "7.x-1.x"
projects[uw_nav_global_footer][subdir] = "features"

; Global Header
projects[uw_nav_global_header][type] = "module"
projects[uw_nav_global_header][download][type] = "git"
projects[uw_nav_global_header][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_global_header.git"
projects[uw_nav_global_header][download][branch] = "7.x-1.x"
projects[uw_nav_global_header][subdir] = "features"

; Main Menu
projects[uw_nav_main_menu][type] = "module"
projects[uw_nav_main_menu][download][type] = "git"
projects[uw_nav_main_menu][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_main_menu.git"
projects[uw_nav_main_menu][download][branch] = "7.x-1.x"
projects[uw_nav_main_menu][subdir] = "features"

; Menu Operations Permission Config
projects[uw_nav_menu_operations_permission][type] = "module"
projects[uw_nav_menu_operations_permission][download][type] = "git"
projects[uw_nav_menu_operations_permission][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_menu_operations_permission.git"
projects[uw_nav_menu_operations_permission][download][branch] = "7.x-1.x"
projects[uw_nav_menu_operations_permission][subdir] = "features"

; Site Footer
projects[uw_nav_site_footer][type] = "module"
projects[uw_nav_site_footer][download][type] = "git"
projects[uw_nav_site_footer][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_site_footer.git"
projects[uw_nav_site_footer][download][branch] = "7.x-4.x"
projects[uw_nav_site_footer][subdir] = "features"

; Restful Conference
projects[uw_restful_conference][type] = "module"
projects[uw_restful_conference][download][type] = "git"
projects[uw_restful_conference][download][url] = "https://git.uwaterloo.ca/wcms/uw_restful_conference.git"
projects[uw_restful_conference][download][branch] = "7.x-1.x"
projects[uw_restful_conference][subdir] = "features"

; FDSU Roles
projects[uw_roles_fdsu][type] = "module"
projects[uw_roles_fdsu][download][type] = "git"
projects[uw_roles_fdsu][download][url] = "https://git.uwaterloo.ca/wcms/uw_roles_fdsu.git"
projects[uw_roles_fdsu][download][branch] = "7.x-1.x"
projects[uw_roles_fdsu][subdir] = "features"

; FDSU Site Controller
projects[uw_site_fdsu][type] = "module"
projects[uw_site_fdsu][download][type] = "git"
projects[uw_site_fdsu][download][url] = "https://git.uwaterloo.ca/wcms/uw_site_fdsu.git"
projects[uw_site_fdsu][download][branch] = "7.x-2.x"
projects[uw_site_fdsu][subdir] = "features"

; User Management
projects[uw_user_management][type] = "module"
projects[uw_user_management][download][type] = "git"
projects[uw_user_management][download][url] = "https://git.uwaterloo.ca/wcms/uw_user_management.git"
projects[uw_user_management][download][branch] = "7.x-1.x"
projects[uw_user_management][subdir] = "features"

; uWaterloo Calendar
projects[uw_cfg_calendar][type] = "module"
projects[uw_cfg_calendar][download][type] = "git"
projects[uw_cfg_calendar][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_calendar.git"
projects[uw_cfg_calendar][download][branch] = "7.x-1.x"
projects[uw_cfg_calendar][subdir] = "features"

; uWaterloo Value Lists
projects[uw_value_lists][type] = "module"
projects[uw_value_lists][download][type] = "git"
projects[uw_value_lists][download][url] = "https://git.uwaterloo.ca/wcms/uw_value_lists.git"
projects[uw_value_lists][download][branch] = "7.x-1.x"
projects[uw_value_lists][subdir] = "features"

; Audience Vocabulary
projects[uw_vocab_audience][type] = "module"
projects[uw_vocab_audience][download][type] = "git"
projects[uw_vocab_audience][download][url] = "https://git.uwaterloo.ca/wcms/uw_vocab_audience.git"
projects[uw_vocab_audience][download][branch] = "7.x-1.x"
projects[uw_vocab_audience][subdir] = "features"

; Profiles Vocabulary
projects[uw_vocab_profiles][type] = "module"
projects[uw_vocab_profiles][download][type] = "git"
projects[uw_vocab_profiles][download][url] = "https://git.uwaterloo.ca/wcms/uw_vocab_profiles.git"
projects[uw_vocab_profiles][download][branch] = "7.x-1.x"
projects[uw_vocab_profiles][subdir] = "features"

; Term Lock
projects[uw_vocab_term_lock][type] = "module"
projects[uw_vocab_term_lock][download][type] = "git"
projects[uw_vocab_term_lock][download][url] = "https://git.uwaterloo.ca/wcms/uw_vocab_term_lock.git"
projects[uw_vocab_term_lock][download][branch] = "7.x-1.x"
projects[uw_vocab_term_lock][subdir] = "features"

; UWaterloo Theme Vocabulary [Publication]
projects[uw_vocab_uwaterloo_theme][type] = "module"
projects[uw_vocab_uwaterloo_theme][download][type] = "git"
projects[uw_vocab_uwaterloo_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_vocab_uwaterloo_theme.git"
projects[uw_vocab_uwaterloo_theme][download][branch] = "7.x-1.x"
projects[uw_vocab_uwaterloo_theme][subdir] = "features"

; Workbench Config
projects[uw_wb_config][type] = "module"
projects[uw_wb_config][download][type] = "git"
projects[uw_wb_config][download][url] = "https://git.uwaterloo.ca/wcms/uw_wb_config.git"
projects[uw_wb_config][download][branch] = "7.x-1.x"
projects[uw_wb_config][subdir] = "features"

; CKEditor Config
projects[uw_wysiwyg_ckeditor][type] = "module"
projects[uw_wysiwyg_ckeditor][download][type] = "git"
projects[uw_wysiwyg_ckeditor][download][url] = "https://git.uwaterloo.ca/wcms/uw_wysiwyg_ckeditor.git"
projects[uw_wysiwyg_ckeditor][download][branch] = "7.x-3.x"
projects[uw_wysiwyg_ckeditor][subdir] = "features"

; XML Sitemap Config
projects[uw_xml_sitemap_config][type] = "module"
projects[uw_xml_sitemap_config][download][type] = "git"
projects[uw_xml_sitemap_config][download][url] = "https://git.uwaterloo.ca/wcms/uw_xml_sitemap_config.git"
projects[uw_xml_sitemap_config][download][branch] = "7.x-1.x"
projects[uw_xml_sitemap_config][subdir] = "features"


;------------------------------------------------------------------------------------------------------------------------
; Themes
;------------------------------------------------------------------------------------------------------------------------

; Adminimal Theme
; Base: 7.x-2.6
; Patch e5cb415c: restore tabledrag icon. https://www.drupal.org/project/adminimal_theme/issues/3061105#comment-13214922
projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][download][type] = "git"
projects[adminimal_theme][download][url] = "https://git.uwaterloo.ca/drupal-org/adminimal_theme.git"
projects[adminimal_theme][download][tag] = "7.x-1.26-uw_wcms1"

; uWaterloo Admin Theme
projects[uw_admin_theme][type] = "theme"
projects[uw_admin_theme][download][type] = "git"
projects[uw_admin_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_admin_theme.git"
projects[uw_admin_theme][download][branch] = "7.x-1.x"

; uWaterloo Adminimal Theme
projects[uw_adminimal_theme][type] = "theme"
projects[uw_adminimal_theme][download][type] = "git"
projects[uw_adminimal_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_adminimal_theme.git"
projects[uw_adminimal_theme][download][branch] = "7.x-1.x"

; uWaterloo Conference frontend
projects[uw_conference_frontend][type] = "theme"
projects[uw_conference_frontend][download][type] = "git"
projects[uw_conference_frontend][download][url] = "https://git.uwaterloo.ca/wcms/uw_conference_frontend.git"
projects[uw_conference_frontend][download][branch] = "7.x-1.x"

; uWaterloo Core Theme (base)
projects[uw_core_theme][type] = "theme"
projects[uw_core_theme][download][type] = "git"
projects[uw_core_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_core_theme.git"
projects[uw_core_theme][download][branch] = "7.x-1.x"

; uWaterloo Decoupled Front-end Theme [Publication]
projects[uw_custom_frontend][type] = "theme"
projects[uw_custom_frontend][download][type] = "git"
projects[uw_custom_frontend][download][url] = "https://git.uwaterloo.ca/wcms-decoupled/uw_custom_frontend.git"
projects[uw_custom_frontend][download][branch] = "7.x-1.x"

; uWaterloo FDSU Theme
projects[uw_fdsu_theme][type] = "theme"
projects[uw_fdsu_theme][download][type] = "git"
projects[uw_fdsu_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_fdsu_theme.git"
projects[uw_fdsu_theme][download][branch] = "7.x-3.x"

; uWaterloo FDSU Theme Responsive
projects[uw_fdsu_theme_resp][type] = "theme"
projects[uw_fdsu_theme_resp][download][type] = "git"
projects[uw_fdsu_theme_resp][download][url] = "https://git.uwaterloo.ca/wcms/uw_fdsu_theme_resp.git"
projects[uw_fdsu_theme_resp][download][branch] = "7.x-1.x"

; uWaterloo Conference Theme
projects[uw_theme_conference][type] = "theme"
projects[uw_theme_conference][download][type] = "git"
projects[uw_theme_conference][download][url] = "https://git.uwaterloo.ca/wcms/uw_theme_conference.git"
projects[uw_theme_conference][download][branch] = "7.x-1.x"

; uWaterloo Single Page Theme
projects[uw_theme_marketing][type] = "theme"
projects[uw_theme_marketing][download][type] = "git"
projects[uw_theme_marketing][download][url] = "https://git.uwaterloo.ca/wcms/uw_theme_marketing.git"
projects[uw_theme_marketing][download][branch] = "7.x-1.x"

; uWaterloo Publication Theme [Publication]
projects[uw_theme_publication][type] = "theme"
projects[uw_theme_publication][download][type] = "git"
projects[uw_theme_publication][download][url] = "https://git.uwaterloo.ca/wcms/uw_theme_publication.git"
projects[uw_theme_publication][download][branch] = "7.x-2.x"


;------------------------------------------------------------------------------------------------------------------------
; Libraries
;------------------------------------------------------------------------------------------------------------------------

; Accessibility Checker
; https://ckeditor.com/cke4/addon/a11ychecker
libraries[a11ychecker][download][type] = "git"
libraries[a11ychecker][download][url] = "https://git.uwaterloo.ca/libraries/a11ychecker.git"
libraries[a11ychecker][download][tag] = "1.1.1"
libraries[a11ychecker][directory_name] = "a11ychecker"

; Backbone.js
; http://backbonejs.org/
; Base: 1.2.1
; Patch 8e7bf66: Aggregation breaks use of library min.map files from included libraries. https://www.drupal.org/node/2235299
libraries[backbonejs][download][type] = "git"
libraries[backbonejs][download][url] = "https://git.uwaterloo.ca/libraries/backbonejs.git"
libraries[backbonejs][download][tag] = "1.2.1-uw_wcms1"
libraries[backbonejs][directory_name] = "backbone"

; Balloon Panel
; (used by Accessibility Checker)
; https://ckeditor.com/cke4/addon/balloonpanel
libraries[balloonpanel][download][type] = "git"
libraries[balloonpanel][download][url] = "https://git.uwaterloo.ca/libraries/balloonpanel.git"
libraries[balloonpanel][download][tag] = "4.13.1"
libraries[balloonpanel][directory_name] = "balloonpanel"

; phpCAS
; https://github.com/Jasig/phpCAS
libraries[CAS][download][type] = "git"
libraries[CAS][download][url] = "https://git.uwaterloo.ca/libraries/phpcas.git"
libraries[CAS][download][tag] = "1.3.8"
libraries[CAS][directory_name] = "CAS"

; CKEditor
; http://ckeditor.com/download
libraries[ckeditor][download][type] = "git"
libraries[ckeditor][download][url] = "https://git.uwaterloo.ca/libraries/ckeditor.git"
libraries[ckeditor][download][tag] = "4.16.1"
libraries[ckeditor][directory_name] = "ckeditor"

; CKEditor CodeMirror Plugin
; https://github.com/w8tcha/CKEditor-CodeMirror-Plugin
libraries[codemirror][download][type] = "git"
libraries[codemirror][download][url] = "https://git.uwaterloo.ca/libraries/CKEditor-CodeMirror-Plugin.git"
libraries[codemirror][download][tag] = "v1.17.12"
libraries[codemirror][directory_name] = "codemirror"

; Colorbox
; http://www.jacklmoore.com/colorbox/
; Base: 1.6.4
; Patch #832. Issue #765. Colorbox displays empty buttons causing an accessibility issue. https://github.com/jackmoore/colorbox/issues/765
libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "https://git.uwaterloo.ca/libraries/colorbox.git"
libraries[colorbox][download][tag] = "1.6.4-uw_wcms1"

; ColorPicker
; http://www.eyecon.ro/colorpicker/
; Used by jquery_colorpicker module.
libraries[colorpicker][download][type] = "git"
libraries[colorpicker][download][url] = "https://git.uwaterloo.ca/libraries/colorpicker.git"
libraries[colorpicker][download][tag] = "2009-05-23"
libraries[colorpicker][directory_name] = "colorpicker"

; d3
; http://d3js.org/
libraries[d3][download][type] = "git"
libraries[d3][download][url] = "https://git.uwaterloo.ca/libraries/d3.git"
libraries[d3][download][branch] = "master"
libraries[d3][download][revision] = "5b981a18"

; iCalcreator
libraries[iCalcreator][download][type] = "git"
libraries[iCalcreator][download][url] = "https://git.uwaterloo.ca/libraries/icalcreator.git"
libraries[iCalcreator][download][tag] = "v2.20.4"
libraries[iCalcreator][directory_name] = "iCalcreator"

; JQuery Cycle
; http://jquery.malsup.com/cycle/
libraries[jquery.cycle][download][type] = "git"
libraries[jquery.cycle][download][url] = "https://git.uwaterloo.ca/libraries/jquery.cycle.git"
libraries[jquery.cycle][download][tag] = "cycle2"
libraries[jquery.cycle][directory_name] = "jquery.cycle"

; Leaflet
; https://leafletjs.com/
libraries[leaflet][download][type] = "git"
libraries[leaflet][download][url] = "https://git.uwaterloo.ca/libraries/leaflet.git"
libraries[leaflet][download][branch] = "0.7.7"
libraries[leaflet][directory_name] = "leaflet"

; Modernizr
; http://modernizr.com
libraries[modernizr][download][type] = "git"
libraries[modernizr][download][url] = "https://git.uwaterloo.ca/libraries/modernizr.git"
libraries[modernizr][download][tag] = "2.8.3"
libraries[modernizr][directory_name] = "modernizr"

; Owl Carousel
; http://owlgraphic.com/owlcarousel/
libraries[OwlCarousel][download][type] = "git"
libraries[OwlCarousel][download][url] = "https://git.uwaterloo.ca/libraries/OwlCarousel.git"
libraries[OwlCarousel][download][tag] = "1.3.2-uw_wcms1"

; Spyc
; https://github.com/mustangostang/spyc.git
libraries[spyc][download][type] = "git"
libraries[spyc][download][url] = "https://git.uwaterloo.ca/libraries/spyc.git"
libraries[spyc][download][tag] = "0.6.3"

; CKEditor Text Selection Plugin
; https://github.com/w8tcha/CKEditor-TextSelection-Plugin/
libraries[textselection][download][type] = "git"
libraries[textselection][download][url] = "https://git.uwaterloo.ca/libraries/CKEditor-TextSelection-Plugin.git"
libraries[textselection][download][tag] = "v1.08"
libraries[textselection][directory_name] = "textselection"

; Timeline JS
; https://github.com/VeriteCo/TimelineJS
; Base: 2.36.0
; Patch 97daed8: Support @alt for images. https://github.com/NUKnightLab/TimelineJS/pull/818
libraries[timelinejs][download][type] = "git"
libraries[timelinejs][download][url] = "https://git.uwaterloo.ca/libraries/timelinejs.git"
libraries[timelinejs][download][tag] = "2.36.0-uw_wcms1"
libraries[timelinejs][directory_name] = "timeline"

; Underscore.js
; http://underscorejs.org
; Base: 1.8.3
; Patch: 2b7f709: Aggregation breaks use of library min.map files from included libraries. https://www.drupal.org/node/2235299
libraries[underscorejs][download][type] = "git"
libraries[underscorejs][download][url] = "https://git.uwaterloo.ca/libraries/underscorejs.git"
libraries[underscorejs][download][tag] = "1.8.3-uw_wcms1"
libraries[underscorejs][directory_name] = "underscore"

; Gmap Markers
; Adds the "university" marker
libraries[gmap_markers][download][type] = "git"
libraries[gmap_markers][download][url] = https://git.uwaterloo.ca/libraries/gmap_markers.git
libraries[gmap_markers][directory_name] = "gmap_markers"
